SHELL = /bin/bash

GITVER = $(shell git describe --always --dirty)
VERSION = $(GITVER)

ifeq ($(VERSION),)
# mw.rev is created after an export so we can rely on it to provide the version here
VERSION = $(shell cat $(SRCROOT)/mw.rev)
endif

MWVERSION = mw3-$(VERSION)

prefix ?= /usr
libdir ?= $(prefix)/lib
bindir ?= $(prefix)/bin
datadir ?= $(prefix)/share
localstatedir ?= /var
initddir ?= /etc/init.d

# The non source files that should get installed
INSTALLFILES = colour help login.banner scripthelp talkhelp wizhelp COPYING INSTALL LICENSE README

LOGDIR := $(localstatedir)/log/mw
MSGDIR := $(localstatedir)/run/mw
STATEDIR := $(localstatedir)/lib/mw
HOMEPATH := $(libdir)/mw

GCCMAJOR := $(echo __GNUC__ | $compiler -E -xc - | tail -n 1)
GCCMINOR := $(echo __GNUC_MINOR__ | $compiler -E -xc - | tail -n 1)
GCCVER := $(shell printf "%02d%02d" $(GCCMAJOR) $(GCCMINOR))

JSDIR=$(DEPTH)mozjs/installroot
JSFLAGS=-include $(JSDIR)/usr/include/js-17.0/js/RequiredDefines.h -I/usr/include/nspr -I$(JSDIR)/usr/include/js-17.0
JSOBJ=$(DEPTH)mozjs/installroot/usr/lib/libmozjs-17.0.a
JSSCRIPTTYPE=JSScript

# cflags for standard 'cc' compiler
CFLAGS+= -Wall -Wshadow -Wmissing-prototypes -Wpointer-arith -Wwrite-strings -Wcast-align -Wbad-function-cast -Wmissing-format-attribute -Wformat=2 -Wformat-security -Wformat-nonliteral -Wno-long-long -Wno-strict-aliasing -pedantic -std=gnu99 -D_GNU_SOURCE $(JSFLAGS) -DJSSCRIPTTYPE=$(JSSCRIPTTYPE)

# until gcc catches up (4.7.x is good)
CFLAGS += $(shell if [ $(GCCVER) -lt 0406 ] ; then echo "-Wno-multichar"; fi )

ifdef WITHPIE
CFLAGS += -fpie
LDFLAGS += -pie
endif

# info strings, do not edit.
DEFS:= -DBUILD_DATE=\"$(shell date +%Y%m%d)\"
DEFS+= -DBUILD_USER=\"$(shell whoami | sed 's/^./\u&/')\"
DEFS+= -DVERSION=\"$(VERSION)\"
DEFS+= -DHOMEPATH=\"$(HOMEPATH)\"
DEFS+= -DLOGDIR=\"$(LOGDIR)\"
DEFS+= -DSTATEDIR=\"$(STATEDIR)\"
DEFS+= -DMSGDIR=\"$(MSGDIR)\"

### uncomment for gdb debugging
LDFLAGS+= -ggdb -g
CFLAGS+= -ggdb -g -D__NO_STRING_INLINE -fstack-protector-all

### Optimisation - uncomment for release & extra testing
CFLAGS+=-O0 -Werror

CFLAGS += $(DEFS)

### The magic which lets us autogenerate dependencies
CFLAGS += -MMD

CODE=$(wildcard *.c)
HDRS=$(wildcard *.h)

all: build

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

-include $(CODE:.c=.d)

$(JSOBJ):
	$(MAKE) -C $(SRCROOT)/mozjs libdir=/usr/lib

.PHONY: build install clean test all testclean


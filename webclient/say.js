var die=0;


function pageInit() {
	sendCmdHandle('who', drawWhoList);
	sendCmdHandle('whoami', userProfile);
	var sayit = document.getElementById("sayit");
	sayit.focus();
}

	function addmsg(type, msg) {
		var tmo = 500;

		if (type != "poll") {
			$("#textlist").append( "<div class='msg"+type+"'>"+msg+ "</div");
		} else
		if (msg.status) {
			tmo = 10000;
			if (msg.status == "Socket open error") {
				$("#textlist").append( "<div class='msg"+type+"'><b><i>Session Closed</i></b></div");
				tmo = 0;
			}else {
				$("#textlist").append( "<div class='msg"+type+"'>Problem: " + msg.status + "</div");
			}
		} else {
			for (one in msg) {
				switch (msg[one].state) {
				case "SAYR":
                case "SAYU":
					// decode the json message
					var detail = JSON.parse(msg[one].text);

					var body = detail.text;
					// make a crude approximation of the old message styles
					switch (detail.type) {
						case "say":
							body = msg[one].name + ": " + detail.text;
							break;
						case "raw":
							body = detail.text;
							break;
						case "emote":
							switch (detail.plural){
								case 1:
									body = msg[one].name + "'s " + detail.text;
									break;
								case 2:
									body = msg[one].name + "' " + detail.text;
									break;
								case 3:
									body = msg[one].name + "'d " + detail.text;
									break;
								case 4:
									body = msg[one].name + "'ll " + detail.text;
									break;
								default:
									body = msg[one].name + " " + detail.text;
									break;
							}
							break;
						case "notsayto":
							body = msg[one].name + " says (-" + detail.exclude + "): " + detail.text;
							break;
						case "says":
							body = msg[one].name + " says: " + detail.text;
							break;
						case "whispers":
							body = msg[one].name + " whispers: " + detail.text;
							break;
						default:
							body = msg[one].name + " " + detail.text;
							break;
					}

					/* Escape HTML characters */
					var escapedMsg = body.replace(/&/g, "&amp;");
					escapedMsg = escapedMsg.replace(/</g, "&lt;");
					escapedMsg = escapedMsg.replace(/>/g, "&gt;");
					escapedMsg = escapedMsg.replace(/\n/g, "");
					escapedMsg = escapedMsg.replace(/\r/g, "");

					/* Detect URIs and convert to links */
					var uris = escapedMsg.match(/https?:\/\/[^ ]*/g);
					for (thisuri in uris) {
						escapedMsg = escapedMsg.replace(uris[thisuri], "<a target=\"_new\" href=\""+uris[thisuri]+"\">"+uris[thisuri]+"</a>");
					}

					/* Insert timestamp if necessary */
					var timestamp="";
					if (userProfile.special.match("t")) {
						var currentTime = new Date();
						var withts=" withts";
						timestamp = "<span class=\"timestamp\">"+pad(currentTime.getHours(), 2)+":"+pad(currentTime.getMinutes(), 2)+" </span>";
					}

					/* Detect username */
					if (escapedMsg.substr(0, msg[one].name.length)==msg[one].name) {
						escapedMsg = escapedMsg.replace(msg[one].name, "<span class='msg_poster'>"+msg[one].name+"</span><span class='msg_content"+withts+"'><span>");
						escapedMsg = escapedMsg+"</span></span>";
					}
				
					/* Replace colour codes with appropriate span tags */				
					var msgColours = escapedMsg.match(/\u001b../g);
					if (msgColours != null) {
						for (i=0;i<msgColours.length;i++) {
							if (isNaN(msgColours[i].charAt(1))) {
								var colourBold = "";
								var fgColour = msgColours[i].charAt(1);
								if (fgColour!=fgColour.toLowerCase()) { colourBold = " bold"; }
								escapedMsg = escapedMsg.replace(msgColours[i], "</span><span class='colourfg"+msgColours[i].charAt(1)+colourBold+" colourbg"+msgColours[i].charAt(2)+"'>");
							} else {
								escapedMsg = escapedMsg.replace(msgColours[i], "</span><span class='colour"+msgColours[i].replace(/\u001b/g, "")+"'>");
							}
						}
					}

					/* Output the processed line */
					$("#textlist").append( "<p class='msg"+type+" user_"+userIndex[msg[one].name.toLowerCase()]+"'>" + timestamp + escapedMsg + "</p");

					break;

				case 11: // Talker message
				case 17: // Board Message
				/* Escape HTML characters */
					var escapedMsg = msg[one].text.replace(/&/g, "&amp;");
					escapedMsg = escapedMsg.replace(/</g, "&lt;");
					escapedMsg = escapedMsg.replace(/>/g, "&gt;");
					escapedMsg = escapedMsg.replace(/\n/g, "");
					escapedMsg = escapedMsg.replace(/\r/g, "");

				/* Detect URIs and convert to links */
					var uris = escapedMsg.match(/https?:\/\/[^ ]*/g);
					for (thisuri in uris) {
						escapedMsg = escapedMsg.replace(uris[thisuri], "<a target=\"_new\" href=\""+uris[thisuri]+"\">"+uris[thisuri]+"</a>");
					}

				/* Insert timestamp if necessary */
					var timestamp="";
					if (userProfile.special.match("t")) {
						var currentTime = new Date();
						var withts=" withts";
						timestamp = "<span class=\"timestamp\">"+pad(currentTime.getHours(), 2)+":"+pad(currentTime.getMinutes(), 2)+" </span>";
					}

				/* Detect username */
					if (escapedMsg.substr(0, msg[one].name.length)==msg[one].name) {
						escapedMsg = escapedMsg.replace(msg[one].name, "<span class='msg_poster'>"+msg[one].name+"</span><span class='msg_content"+withts+"'><span>");
						escapedMsg = escapedMsg+"</span></span>";
					}

				/* Replace colour codes with appropriate span tags */				
					var msgColours = escapedMsg.match(/\u001b../g);
					if (msgColours != null) {
						for (i=0;i<msgColours.length;i++) {
							if (isNaN(msgColours[i].charAt(1))) {
								var colourBold = "";
								var fgColour = msgColours[i].charAt(1);
								if (fgColour!=fgColour.toLowerCase()) { colourBold = " bold"; }
								escapedMsg = escapedMsg.replace(msgColours[i], "</span><span class='colourfg"+msgColours[i].charAt(1)+colourBold+" colourbg"+msgColours[i].charAt(2)+"'>");
							} else {
								escapedMsg = escapedMsg.replace(msgColours[i], "</span><span class='colour"+msgColours[i].replace(/\u001b/g, "")+"'>");
							}
						}
					}

				/* Output the processed line */
					$("#textlist").append( "<p class='msg"+type+" user_"+userIndex[msg[one].name.toLowerCase()]+"'>" + timestamp + escapedMsg + "</p");

					break;
				case 14: // IPC_KICK
					var what = "<div class='msgkick user_system'>";
					if (msg[one].text.substr(0,1) == "m") {
						what += "Boing, Zebedee's arrived. \"Look up!\" says Zebedee<br />";
						what += "You look up; a large object is falling towards you very fast, very very fast. It looks like a Magic Roundabout!<br />";
						what += "\"I wouldn't stand there if I was you\" says Zebedee<br />";
						what += "WWWHHHEEEEEEEKKKKEEEERRRRRUUUUUNNNNNCCCCCHHHHHH<br />";
						what += msg[one].name + " has just dropped the Magic Roundabout of Death on you.<br />";
					} else {
						what += "Boing, Zebedee arrived.<br />";
						what += "\"Time for bed\" says Zebedee<br />";
						what += msg[one].name + " has just dropped the Zebedee of Death on you.<br />";
					}
					if (msg[one].text.substr(1,1) == 'r') what += "\""+msg[one].text.substr(2)+"\" says Zebedee<br />";
					what += "</div>";
					$("#textlist").append(what);
					break;
				case 23: // CheckOnOff
					sendCmdHandle('who', drawWhoList);
					break;
				default: 
					what = "<div class='msg"+type+"'>";
					what += "Unknown message type '"+msg[one].state+"' body='"+msg[one].text+"'";
					what += "</div>";
					$("#textlist").append(what);
					break;
				}
			}
		}
		var objDiv = document.getElementById("textlist");
		if (objDiv!=null) objDiv.scrollTop = objDiv.scrollHeight;
		return tmo;
	}

	function waitForMsg() {
		$.ajax( {
			type: "GET",
			url: "poll.php",
			async: true,
			cache: false,
			timeout: 50000,
			success: function(data) {
				var tmo = addmsg("poll", data);
				if (tmo>0) setTimeout( 'waitForMsg()', tmo);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				var tmo = addmsg("error", textStatus+" ("+errorThrown+")");
				if (tmo>0) setTimeout('waitForMsg()', tmo);
			}
		});
	}

function sendSay(text)
{
	if (text.substr(0, 1)=="."||text.substr(0, 1)=="!"||text.substr(0, 1)=="/") {
		cmdParser(text);
		return false;
	}

	var what = "say "+text;
	$.getJSON("send.php", {send: what}, function(data, stats) {
		});
	return false;
}

function cmdParser(text) {
	if (text.search(" ")==-1) {
		var cmd = text.substring(1);
	} else {
		var cmd = text.substring(1, text.search(" "));
		var args = text.substring(text.search(" ")+1);
	}

	switch(cmd) {
	case "who":
		sendCmdHandle('who', drawWho);
		break;
	case "room":
                if (args == undefined){
                        $('#textlist').append("<div class='error'>Usage: "+cmd+" &lt;number&gt;</div>");
                }else{
			sendCmd('channel '+args);
                }
		break;
	case "e":
	case "em":
	case "emote":
	case "me":
		var what = "emote "+args;
		if (args == undefined){
			$('#textlist').append("<div class='error'>Usage: "+cmd+" &lt;text&gt;</div>");
		}else{
	                $.getJSON("send.php", {send: what});
		}
		break;
	case "sayto":
	case "sa":
	case "whisper":
	case "whi":
		var what = "sayto "+args;
                if (args == undefined){
			$('#textlist').append("<div class='error'>Usage: "+cmd+" &lt;user&gt; &lt;text&gt;</div>");
		}else{
		        $.getJSON("send.php", {send: what});
		}

		break;
	case "q":
	case "qq":
	case "quit":
		sendCmd('quit');
		window.location = 'index.php?action=logout'
		break;
	}
}

function drawTime(time) {
	var days = time/86400;
	var hours = (time%86400)/3600;
	var minutes = ((time%86400)%3600)/60;
	var seconds = ((time%86400)%3600)%60;
	var text = "";
	if (days>=2) text += parseInt(days)+"d";
	if (hours>=1) text += parseInt(hours)+"h";
	if (minutes>=1&&days<2) text += parseInt(minutes)+"m";
	if (hours<1) text += seconds+"s";
	return text;
}

function drawWho(data, stat)
{
        var text = "<hr /><table cellspacing=\"0\" cellpadding=\"0\"><tbody>";
        text += "<tr><th style=\"width: 10em;\">Name</th><th style=\"width: 6em;\">Idle</th><th>What...</th></tr>";
        for (person in data) {
		var line = "<td>"+data[person].name+"</td>";
		line += "<td>"+drawTime(data[person].idletime)+"</td>";
		line += "<td>Room "+data[person].channel+"</td>";
		text += "<tr>"+line+"</tr>";
        }
        text += "</tbody></table><hr />";
        addmsg("who", text);
}

function sortWho(a, b) {
	var nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase();
	if (nameA < nameB) return -1;
	if (nameA > nameB) return 1;
	return 0;
}

// For debugging purposes
function concatObject(obj) {
  str='';
  for(prop in obj)
  {
    str+=prop + " => "+ obj[prop]+"\n";
  }
  return(str);
}

function userInit(data, stat) {
	userIndex=new Object;
	data.sort(sortWho);
	usercount=0;
	for (var person in data) {
		var thisUsername=data[person].name.toLowerCase();
		if (thisUsername in userIndex) usercount--;
		userIndex[thisUsername]=usercount;
		usercount++;
	}
}

function userAdd(data, stat) {
	for (var person in data) {
		var thisUsername=data[person].name.toLowerCase();
		if (!(thisUsername in userIndex)) {
			userIndex[thisUsername]=usercount;
			usercount++;
		}
	}
}

function drawWhoList(data, stat) {
	var idleness;
	if(typeof userIndex == "undefined") userInit(data, stat);
	else userAdd(data, stat);

	$("li").remove();
	data.sort(sortWho);
	for (var person in data) {
		idleness="";
		if (data[person].idletime>1800) idleness=" idle";
		var thisUsername=data[person].name.toLowerCase();
		var personinfo = "<div class=\"whoinfo\">";
		if (data[person].hgwidth>0) personinfo += "<img src=\"https://sucs.org/pictures/people/"+data[person].name.toLowerCase()+".png\" width=\""+data[person].hgwidth+"\" height=\"64\" />";
		else personinfo += "<img src=\"person.png\" width=\"64\" height=\"64\" />";
		personinfo += "<strong>"+data[person].name+"</strong>";
		personinfo += " "+data[person].doing;
		personinfo += "<br />";
		if (data[person].realname!=undefined) personinfo += data[person].realname+"<br />";
		personinfo += "Room "+data[person].channel;
		personinfo += "</div>";
		$("#wholist").append("<li class=\"who user_"+userIndex[thisUsername]+idleness+"\"><a href=\"#\">"+data[person].name+personinfo+"</a></li>");
	}
}

function userProfile(data, stat) {
	userProfile=new Object;
	for (var item in data) {
		userProfile[item] = data[item];
	}
}

function sendCmdHandle(cmd, handle)
{
	$.getJSON("send.php", {send: cmd}, handle);
}

function sendCmd(cmd) {
	$.getJSON("send.php", {send: cmd});
}

function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

function submitenter(myfield,e) {
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;
	switch (keycode) {
	case 13:
		var text = $('#sayit').val();
		$('#sayit').val("");
		sendSay(text);
		return false;
	case 27:
		insertEscape(false);
		return false;
	default:
		return true;
	}
}

function insertEscape(buttonpress) {
var myField=document.getElementById("sayit");
var myValue="\033";
  //IE support
  if (document.selection) {
    myField.focus();
    sel = document.selection.createRange();
    sel.text = myValue;
  }
  //MOZILLA/NETSCAPE support
  else if (myField.selectionStart || myField.selectionStart == '0') {
    var startPos = myField.selectionStart;
    var endPos = myField.selectionEnd;
    myField.value = myField.value.substring(0, startPos)
                  + myValue
                  + myField.value.substring(endPos, myField.value.length);
    myField.selectionStart = startPos+1;
    myField.selectionEnd = myField.selectionStart;
  } else {
    myField.value += myValue;
  }
	if (buttonpress) myField.focus();
}

$(document).ready(function(){waitForMsg();});

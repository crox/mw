<?
# Force the user to use HTTPS
if (empty($_SERVER['HTTPS'])) header("Location: https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);

define('SMARTY_DIR', '/usr/share/php/smarty/libs/');
require("/usr/share/php/smarty/libs/Smarty.class.php");

$smarty = new Smarty;
$smarty->assign("self", basename($_SERVER['PHP_SELF']));

$mwsess = @$_COOKIE['mwsess'];
$action = @$_REQUEST['action'];

if (isset($_GET['mwsess'])) {
	$mwsess = $_GET['mwsess'];
	setcookie("mwsess", $mwsess);
}

if ($action == "logout") {
	setcookie("mwsess", "");
	unset($mwsess);
}
if ($action == "login" || $action == "create") {
require("startup.php");
}

if (!isset($mwsess)) {
	if (@$_REQUEST['sucssite_loggedin']=="true" && $action!="logout") {
		require("startup.php");
	} else {
		$smarty->assign("body", $smarty->fetch("login.html"));
		$smarty->display("main.html");
		exit;
	}
}

$session = unserialize($mwsess);

$smarty->assign("body", $smarty->fetch("front.html"));
$smarty->display("main.html");

?>

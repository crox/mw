#ifndef HTML_RENDER_H
#define HTML_RENDER_H

#include <obstack.h>

struct html_doc_head {
	struct html_object *obj;
	struct obstack stack;
	FILE *fp;
};

struct html_object {
	struct html_object *next;
	void (*start)(struct html_doc_head *hdh, struct html_object *);
	struct html_object *contents;
	struct html_object *last_contents;
	void (*stop)(struct html_doc_head *hdh, struct html_object *);
	void *start_data;
	void *stop_data;
};

extern int render_init(struct html_doc_head *hdh);
extern void __render(struct html_doc_head *hdh, struct html_object *);
extern struct html_object *render_alloc(struct html_doc_head *hdh, struct html_object *container);

static __inline__ struct html_object *html_tag(struct html_doc_head *hdh, char *start, char *end, struct html_object *container)
{
        struct html_object *o = render_alloc(hdh, container);
        o->start_data = start;
        o->stop_data = end;

        return o;
}


extern int html_fdcat(struct html_doc_head *hdh, int fd, int len);

static __inline__ void html_strcat(struct html_doc_head *hdh, char *str)
{
	obstack_grow(&hdh->stack, str, strlen(str));
}



static __inline__ void html_strncat(struct html_doc_head *hdh, char *str, int len)
{
	obstack_grow(&hdh->stack, str, len);
}

extern void html_esccat(struct html_doc_head *hdh, char *str);

static __inline__ void html_intcat(struct html_doc_head *hdh, int arg)
{
	char buf[16];
	snprintf(buf, 16, "%d", arg);
	obstack_grow(&hdh->stack, buf, strlen(buf));
}

static __inline__ char *html_strdone(struct html_doc_head *hdh)
{
	obstack_1grow(&hdh->stack, 0);
	return (char *)obstack_finish(&hdh->stack);
}

static __inline__ struct html_object *html_text(struct html_doc_head *hdh, char *txt, struct html_object *container)
{
        html_strcat(hdh, txt);
        return html_tag(hdh, html_strdone(hdh), NULL, container);
}

static __inline__ void render(struct html_doc_head *hdh)
{
	__render(hdh, hdh->obj);
}

static __inline__ void render_done(struct html_doc_head *hdh)
{
	obstack_free(&hdh->stack, NULL);
}

#endif /* HTML_RENDER_H */

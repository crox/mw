#define _BSD_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

struct cgi_object {
	char *name;
	char *value;
	
	struct cgi_object *next;
};

static struct cgi_object *cookie_main_list=NULL;

/* dont need to decode cookies */
static void url_decode(char *dst, char *src, int len)
{
	strncpy(dst,src,len);
	dst[len]=0;
}

/* private function to split the query string up */
/* destructive to the query string */
static int cookie_split_pairs(char *query)
{
	char *start, *mid, *end;
	struct cgi_object *new;
	int count=0;

	start=query;
	while (start!=NULL && *start!=0)
	{
		int len;

		while (*start==' ') start++;
		if ((end=strchr(start, ';'))==NULL)
			end=start+strlen(start);

		if ((mid=strchr(start, '='))==NULL)
			mid=end;
	
		if (mid > end) mid=end;

		if (start == mid) 
		{
			start=end+1;
			continue;
		}

		new=(struct cgi_object *)malloc(sizeof(*new));
		len=mid-start;
		new->name=(char *)malloc(len+1);
		url_decode(new->name, start, len);
	
		if (end > mid)
		{
			mid++;
			len=end-mid;
			new->value=(char *)malloc(len+1);
			url_decode(new->value, mid, len);
			new->value[len]=0;
		}else
			new->value=NULL;
	
		new->next=cookie_main_list;
		cookie_main_list=new;
		start=end;
		if (*start==' ') start++;
#ifdef DEBUG
		printf(stderr, "%d: start=0x%x '%c', mid=0x%x '%c', end=0x%x '%c'\n", count, start, *start, mid, *mid, end, *end);
#endif
		count++;
	}
	return (count );
}	
			

char *cookie_find(char *what)
{
	struct cgi_object *ptr;

	ptr=cookie_main_list;
	while (ptr!=NULL)
	{
		if (!strcasecmp(what, ptr->name))
			return(ptr->value);
		ptr=ptr->next;
	}
	return(NULL);
}

/* count the number of arguments,
   or number of arguments with a specific name */
int cookie_count(char *what)
{
	struct cgi_object *ptr;
	int count=0;

	ptr=cookie_main_list;
	while (ptr!=NULL)
	{
		if (what==NULL || !strcmp(what, ptr->name))
			count++;
		ptr=ptr->next;
	}
	return (count);
}

/* master processing function */
int cookie_process(void)
{
	static int done=0;
	char *query;
	int count=0;

	if (done) return ( cookie_count(NULL) );

	if ((query=getenv("HTTP_COOKIE"))==NULL)
		return(0);

	query=strdup(query);
	count=cookie_split_pairs(query);	
	free(query);

	done=1;
	return ( count );
}

#ifdef TEST

int main()
{
	int i;
	struct cgi_object *ptr;

	i=cookie_process();
	
	printf("Content-type: text/html\n\n");
	printf("<html><body>\n");
	printf("Got %d cookies<br>\n",i);

	if (i>0)
	{
		printf("<ol>\n");
		ptr=cookie_main_list;
		while (ptr!=NULL)
		{
			printf("<li> '%s'", ptr->name);
			if (ptr->value!=NULL)
				printf(" = '%s'", ptr->value);
			printf("\n");
			ptr=ptr->next;
		}
		printf("</ol>\n");
	}
	printf("</body></html>\n");
	exit(0);
}

#endif		

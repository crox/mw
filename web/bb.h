#define FOLDERFILE 	"folders.bb"
#define USERFILE 	"users.bb"
#define LOGFILE		"log.bb"
#define LOGIN_BANNER	"login.banner"
#define LOCKFILE	"/tmp/bb.locked"
#define EDITOR		"/usr/bin/vi"
#define HOMEPATH	"/usr/local/lib/mw"
#define HELPDIR		"help"
#define WIZHELP		"wizhelp"
#define SCRIPTHELP	"scripthelp"

#define TEXT_END	".t"
#define INDEX_END	".i"
#define MOD_END		".m"
#define PATHSIZE	256
#define ERRORMSG 	"I don't understand. Type 'help' for help.\n"
#define FALSE 	0
#define TRUE 	!FALSE
#define SETALLLONG 0xFFFFFFFF

#define TIMEOUT		600	/* seconds */

#define NAMESIZE 	16	/* username */
#define PASSWDSIZE 	20	/* password (after encryption) */
#define SUBJECTSIZE 	40	/* Subject field on messages */
#define FOLNAMESIZE 	10	/* length of folder names */
#define TOPICSIZE 	30	/* length of the topic of the folder */
#define REALNAMESIZE	30	/* real name */
#define CONTACTSIZE 	60	/* contact address */
#define LOGLINESIZE	2048	/* size of a line printed to log file */
#define DOINGSIZE	80	/* 'doing' user record field */
#define MAXTEXTLENGTH	1024	/* text buffer size */

#define RUNAWAY		1200
#define FLOODLIMIT	15

struct IgnoreList
{
	int			pid;
	char			*name;
	struct IgnoreList	*next;
};
                
struct NewHeader 
{
	int Ref;
	long date;
	char from[30];
	char to[SUBJECTSIZE+1];
	char subject[SUBJECTSIZE+1];
	char *text;
	int size;
	char status;
	char ident[40]; 
};

struct Header
{
	int Ref;
	long date;
	char from[NAMESIZE+1],to[SUBJECTSIZE+1];
	char subject[SUBJECTSIZE+1];
	long datafield;
	int size;
	char status;
	int replyto;
	char spare[4]; /*adjust size as necessary */
};

/*	Header.status
bit	use
0	moderator approved
1	marked for deletion
2	has a reply
3	is a remote posting
4	-
5	-
6	-
7	-
*/

struct person 
{
	char name[NAMESIZE+1];
	char passwd[PASSWDSIZE+1];
	long lastlogin; 
	long folders[2]; /* which folders are subscribed to */
	unsigned char status;
	unsigned short special;
	int  lastread[64]; /* last message read in each folder */
	char realname[REALNAMESIZE+1];
	char contact[CONTACTSIZE+1];
	long timeused;
	long idletime;
	char groups;
	char doing[DOINGSIZE];
	long timeout;

	unsigned char spare;
	unsigned char colour;
	unsigned short room;
	unsigned long chatprivs;
	unsigned long chatmode;
};

/* person.status
bit	use
0	registered
1	is a moderator
2	is a superuser
3	is banned
4	messages on/off(1=off)
5	inform on/off(1=off)
6	-
7	marked for deletion
*/

struct folder
{
	unsigned char status; /* status for people not in the same group */
	char name[FOLNAMESIZE+1];
	char topic[TOPICSIZE+1];
	int first; /* Ref no of first message in the folder */
	int last; /* Ref no of last message in the folder */
	char g_status; /* status for people in the same group */
	char groups; /* which groups g_status applies to */
	char spare[10];
};

/* folder.status
bit 	use
0	active
1	read by unregistered
2	wrte by unregistered
3	read by registered
4	wrte by registered
5	private folder
6	moderated folder
7	-
*/

struct who 
{
	int pid;
	long posn;
};

typedef struct __dirinfo
{
	char *link;	/* direction command (eg, 'jump') */
	char *from;     /* leave message (eg, 'jumped up') */
	char *to;	/* entering message (eg, 'fallen from') */
	char *leaving;	/* leaving message (eg, 'leaving main hall, falling down to') */
	struct __dirinfo *next;
} DirInfo;

struct room_vis_info
{
	char *name;
	int  rnum;
};

struct room
{
	int num;			/* room number */
	char *name;			/* room name */
	char *desc;			/* description */
	char *prompt;			/* alternate prompt */
	int hidden;			/* cant find room using 'room' command */
	int sproof;			/* soundproof? */
	int locked;			/* locked? */
	int *dir;			/* room numbers can find from here */
	char **link;			/* link text for directions off here */
	char **dirdef;			/* direction name redefs */
	char **fromdef;			/* direction leave redefs */
	char **todef;			/* direction arrive redefs */
	char **leavedef;		/* direction leaving redefs */

	struct room_vis_info *vnames;	/* externally visible names */
};

typedef struct gag_info
{
	char *name;			/* gag filter name */
	char *text;			/* gag realname */
	char *gag;			/* text on gag */
	char *ungag;			/* text on ungag */
	void (*Function)(char *);	/* gag function to call */
} GagInfo;

extern int internet;

#include <unistd.h>
#include <stdlib.h>
#include <time.h>
extern mode_t	umask __P ((mode_t __mask)); /* from unistd.h.old */

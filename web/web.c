#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pwd.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#define HTML_MODE (S_IROTH|S_IXOTH)

static void make_lower(char *str)
{
	while(*str) {
		if (isupper(*str))
			*str = *str - 'A' + 'a';
		str++;
	}
}

int has_web_page(char *name)
{
	struct passwd *pw;
	char path[2048];
	struct stat st;

	strncpy(path, name, 2048);
	make_lower(path);

	if ((pw = getpwnam(path)) == NULL)
		return 0;

	snprintf(path, 2048, "%s/public_html", pw->pw_dir);

	if (stat(path, &st) < 0)
		return 0;

	if (!S_ISDIR(st.st_mode) || ((st.st_mode&HTML_MODE) != HTML_MODE))
		return 0;

	snprintf(path, 2048, "%s/public_html/robots.txt", pw->pw_dir);
	stat(path, &st);

	if (errno != ENOENT)
		return 0;

	return 1;
}

#ifdef TEST
int main(int argc, char *argv[])
{
	int i;

	if (argc < 2) {
		printf("One argument required\n");
		exit(0);
	}

	if ((i = has_web_page(argv[1])))
		printf("%s has a web page\n", argv[1]);

	return i;
}
#endif

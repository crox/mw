/* cgi.c */
int url_decode(char *dst, char *src, int len);
char *cgi_find(char *what);
char *cgi_nfind(char *what, int which);
int cgi_count(char *what);
int cgi_process(void);
/* cookie.c */
int cookie_process(void);
char *cookie_find(char *what);

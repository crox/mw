#define _GNU_SOURCE
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <sysexits.h>

#include "bb.h"
#include "cgi.h"
#include "render.h"
#include "perms.h"
#include "db.h"
#include "persist.h"

extern int has_web_page(char *name);
extern int by_date(struct folder *ff, struct Header *hh, void *arg1, void *arg2);
extern struct Header *date_order_header(struct folder **ffp, struct Header *hh);

struct config {
        struct folder *ff;
        struct person *pp;
        int mesg;
        int set_cookie;
        int failed_passwd;
};

static struct config mw_web_config = { NULL, NULL, -1, 0, 0 };

static int valid_number(struct folder *ff, int n)
{
	if (n <= 0)
		return 0;
	if (n < ff->first)
		return 0;
	if (n > ff->last)
		return 0;

	return 1;
}

static void config2url(struct html_doc_head *hdh, struct config *c)
{
	html_strcat(hdh, "mw.cgi?fol=");
	html_esccat(hdh, c->ff ? c->ff->name : "NEW");

	if (c->mesg > 0) {
		html_strcat(hdh, "&msg=");
		html_intcat(hdh, c->mesg);
	}
}

static struct html_object *link_mw(struct html_doc_head *hdh, struct config *cfg, char *title, struct html_object *container)
{
	html_strcat(hdh, "<A HREF=\"");
	config2url(hdh, cfg);
	if (title) {
		html_strcat(hdh, "\" TITLE=\"");
		html_strcat(hdh, title);
	}
	html_strcat(hdh, "\">");

	return html_tag(hdh, html_strdone(hdh), "</A>", container);
}

static struct html_object *outline_box(struct html_doc_head *hdh, struct html_object *obj)
{
	static char *txt = 
"<TABLE CELLPADDING=2 BGCOLOR=\"#6688FF\" COLS=1 BORDER=0 CELLSPACING=0 "
"HEIGHT=\"99%\" WIDTH=\"99%\">\n"
"<TR><TD>\n"
"<TABLE CELLPADDING=3 BGCOLOR=\"#ffffff\" COLS=1 BORDER=0 CELLSPACING=0>\n"
"<TR><TD>\n";

	return html_tag(hdh, txt, "</TD></TR></TABLE></TD></TR></TABLE>", obj);
}

static struct html_object *shaded_bar(struct html_doc_head *hdh, char *colour, int cols, struct html_object *obj)
{
	struct html_object *o = render_alloc(hdh, obj);

	html_strcat(hdh, "<TR BGCOLOR=\"");
	html_strcat(hdh, colour);
	html_strcat(hdh, "\">\n<TD COLSPAN=");
	html_intcat(hdh, cols);
	html_strcat(hdh, " ALIGN=CENTER>");

	o->start_data = html_strdone(hdh);
	o->stop_data = "</TD></TR>";

	return o;
}

static struct html_object *shaded_title(struct html_doc_head *hdh, char *colour, int cols, struct html_object *obj)
{
	obj = shaded_bar(hdh, colour, cols, obj);
	obj = html_tag(hdh, "<EM>", "</EM>", obj);
	return html_tag(hdh, "<FONT SIZE=+3>", "</FONT>", obj);
}

static char *user_lower(char *str)
{
	static char buf[NAMESIZE];
	char *p = buf;
	strncpy(buf, str, NAMESIZE);
	while(*p) {
		if (isupper(*p))
			*p = *p - 'A' + 'a';
		p++;
	}

	return buf;
}

static void box_message(struct html_doc_head *hdh, struct folder *ff, struct Header *hh, struct html_object *obj)
{
	struct html_object *title, *text, *info, *navi, *cell;
	struct config cfg;
	int fd;
	int hw;

	memcpy(&cfg, &mw_web_config, sizeof(struct config));
	cfg.mesg = -1;
	cfg.ff = ff;

	obj = outline_box(hdh, obj);
	obj = html_tag(hdh, "<TABLE COLS=3>", "</TABLE>", obj);
	title = shaded_title(hdh, "#CCEEFF", 3, obj);

	html_strcat(hdh, "<A HREF=\"");
	config2url(hdh, &cfg);
	html_strcat(hdh, "\">");
	html_strcat(hdh, ff->name);
	html_strcat(hdh, "</A>: Message ");
	html_intcat(hdh, hh->Ref);
	html_strcat(hdh, " from ");
	if ((hw = has_web_page(hh->from)) != 0) {
		html_strcat(hdh, "<A HREF=\"/~");
		html_strcat(hdh, user_lower(hh->from));
		html_strcat(hdh, "/\">");
	}
	html_strcat(hdh, hh->from);
	if (hw != 0) {
		html_strcat(hdh, "</A>");
	}
	html_strcat(hdh, " ");
	html_strcat(hdh, ctime(&hh->date));
	html_tag(hdh, html_strdone(hdh), NULL, title);

	info = html_tag(hdh, "<TR>", "</TR>", obj);
	info = html_tag(hdh, "<TD ALIGN=\"LEFT\" COLSPAN=3>", "</TD>", info);
	info = html_tag(hdh, "<B>", "</B>", info);
	html_strcat(hdh, "To: ");
	if ((hw = has_web_page(hh->to)) != 0) {
		html_strcat(hdh, "<A HREF=\"/~");
		html_strcat(hdh, user_lower(hh->to));
		html_strcat(hdh, "/\">");
	}
	html_strcat(hdh, hh->to);
	if (hw != 0) {
		html_strcat(hdh, "</A>");
	}
	html_tag(hdh, html_strdone(hdh), NULL, info);

	info = html_tag(hdh, "<TR>", "</TR>", obj);
	info = html_tag(hdh, "<TD ALIGN=\"LEFT\" COLSPAN=3>", "</TD>", info);
	info = html_tag(hdh, "<B>", "</B>", info);
	html_strcat(hdh, "Subject: ");
	html_strcat(hdh, hh->subject);
	html_tag(hdh, html_strdone(hdh), NULL, info);

	if (valid_number(ff, hh->replyto)) {
		info = html_tag(hdh, "<TR>", "</TR>", obj);
		info = html_tag(hdh, "<TD ALIGN=\"LEFT\" COLSPAN=3>", "</TD>", info);
		info = html_tag(hdh, "<B>", "</B>", info);
		cfg.mesg = hh->replyto;
		html_strcat(hdh, "In Reply To: <A HREF=\"");
		config2url(hdh, &cfg);
		html_strcat(hdh, "\">Message ");
		html_intcat(hdh, hh->replyto);
		html_strcat(hdh, "</A>");
		html_tag(hdh, html_strdone(hdh), NULL, info);
	}

	text = html_tag(hdh, "<TR>", "</TR>", obj);
	text = html_tag(hdh, "<TD COLSPAN=3>", "</TD>", text);
	text = html_tag(hdh, "<P>", "</P>", text);
	text = html_tag(hdh, "<PRE>", "</PRE>", text);

	fd = open_text(ff, hh);
	if (fd >= 0) {
		html_fdcat(hdh, fd, hh->size);
		html_tag(hdh, html_strdone(hdh), NULL, text);
	}

	navi = html_tag(hdh, "<TR BGCOLOR=\"#CCEEFF\">", "</TR>", obj);
	cell = html_tag(hdh, "<TD ALIGN=\"CENTER\">", "</TD>", navi);
	if (valid_number(ff, hh->Ref-1)) {
		cfg.mesg = hh->Ref-1;
		cell = link_mw(hdh, &cfg, "Previous Message", cell);
		html_tag(hdh, "[Previous Message]", NULL, cell);
	}
	cell = html_tag(hdh, "<TD ALIGN=\"CENTER\">", "</TD>", navi);
	cell = html_tag(hdh, "<TD ALIGN=\"CENTER\">", "</TD>", navi);
	if (valid_number(ff, hh->Ref+1)) {
		cfg.mesg = hh->Ref+1;
		cell = link_mw(hdh, &cfg, "Next Message", cell);
		html_tag(hdh, "[Next Message]", NULL, cell);
	}
}

static void box_welcome(struct html_doc_head *hdh, struct html_object *obj)
{
	struct html_object *cell;

	obj = html_tag(hdh, "<P>", "</P>", obj);
	obj = html_tag(hdh, "<TABLE COLS=1>", "</TABLE>", obj);

	cell = html_tag(hdh, "<TR>", "</TR>", obj);
	cell = html_tag(hdh, "<TD ALIGN=\"CENTER\">", "</TD>", cell);
	html_tag(hdh, "Welcome to Milliways", NULL, cell);

	cell = html_tag(hdh, "<TR>", "</TR>", obj);
	cell = html_tag(hdh, "<TD ALIGN=\"CENTER\">", "</TD>", cell);
	html_tag(hdh, mw_web_config.pp->name, NULL, cell);

	cell = html_tag(hdh, "<TR>", "</TR>", obj);
	cell = html_tag(hdh, "<TD>", "</TD>", cell);

	
}

static void box_passwd(struct html_doc_head *hdh, struct html_object *obj)
{
	struct html_object *row;
	struct html_object *cell;

	obj = html_tag(hdh, "<P>", "</P>", obj);
	obj = html_tag(hdh, "<FORM ACTION=\"mw.cgi\" METHOD=\"POST\">", "</FORM>", obj);
	obj = html_tag(hdh, "<TABLE COLS=2>", "</TABLE>", obj);

	row = html_tag(hdh, "<TR>", "</TR>", obj);
	cell = html_tag(hdh, "<TD>", "</TD>", row);
	html_tag(hdh, "UserName:", NULL, cell);
	cell = html_tag(hdh, "<TD>", "</TD>", row);
	html_tag(hdh, "<INPUT TYPE=TEXT NAME=\"user\" MAXLENGTH=16 TABORDER=1>", NULL, cell);

	row = html_tag(hdh, "<TR>", "</TR>", obj);
	cell = html_tag(hdh, "<TD>", "</TD>", row);
	html_tag(hdh, "Password:", NULL, cell);
	cell = html_tag(hdh, "<TD>", "</TD>", row);
	html_tag(hdh, "<INPUT TYPE=PASSWORD NAME=\"passwd\" MAXLENGTH=20 TABORDER=2>", NULL, cell);

	row = html_tag(hdh, "<TR>", "</TR>", obj);
	cell = html_tag(hdh, "<TD ALIGN=CENTER COLSPAN=2>", "</TD>", row);
	html_tag(hdh, "<INPUT TYPE=SUBMIT VALUE=\"Enter the Board\" TABORDER=3>", NULL, cell);
}

static void box_login(struct html_doc_head *hdh, struct html_object *obj)
{
	if (mw_web_config.set_cookie)
		box_welcome(hdh, obj);
	else
		box_passwd(hdh, obj);
}

static void box_info(struct html_doc_head *hdh, struct html_object *obj)
{
static char *msg = 
	"Milliways is the bulletin board system used by the\n"
	"<A HREF=\"http://www.sucs.swan.ac.uk/\">Swansea University Computer "
	"Society</A> to communicate with its members.\n"
	"It is also open to non-members provided they comply with our rules\n"
	"which are largely based upon those of "
	"<A HREF=\"http://www.ja.net/\">\n"
	"JANET</A>, our network provider and <A HREF="
	"\"http://www.swan.ac.uk/\">Swansea University</A>.\n"
	"This web interface to Milliways is an experimental\n"
	"(read-only) system. To use the system normally telnet to\n"
	"<A HREF=\"telnet://sucs.sucs.swan.ac.uk:5555/\">"
	"sucs.sucs.swan.ac.uk</A> port 5555\n";

	obj = html_tag(hdh, "<P>", "</P>", obj);
	html_tag(hdh, msg, NULL, obj);
}

static void box_options(struct html_doc_head *hdh, struct html_object *obj)
{
	struct html_object *title, *ent;

	obj = outline_box(hdh, obj);
	obj = html_tag(hdh, "<P>", "</P>", obj);
	obj = html_tag(hdh, "<TABLE COLS=2>", "</TABLE>", obj);
	title = shaded_bar(hdh, "#000000", 2, obj);
	title = html_tag(hdh, "<FONT FACE=\"Helvetica,Swiss,Courier\" SIZE=+5 COLOR=\"#ffffff\">", "</FONT>", title);
	html_tag(hdh, "Milliways", NULL, title);

	obj = html_tag(hdh, "<TR>", "</TR>", obj);
	ent = html_tag(hdh, "<TD>", "</TD>", obj);
	box_info(hdh, ent);
	ent = html_tag(hdh, "<TD>", "</TD>", obj);
	box_login(hdh, ent);
}

static void box_folders(struct html_doc_head *hdh, struct html_object *obj)
{
	struct folder *ff;
	struct person *pp = mw_web_config.pp;
	int i = 0, j;
	int f = 0;
	struct html_object *title, *row = NULL, *cols[3];
	struct config cfg;

	obj = outline_box(hdh, obj);
	obj = html_tag(hdh, "<TABLE COLS=3>", "</TABLE>", obj);
	title = shaded_title(hdh, "#CCEEFF", 3, obj);
	html_tag(hdh, "Folders", NULL, title);

	while((ff = folder_by_number(f++)) != NULL) {
		if (!f_active(ff->status))
			continue;
		if (!allowed_r(ff, pp))
			continue;
		if (row == NULL) {
			row = html_tag(hdh, "<TR>", "</TR>", obj);
			for(j = 0; j < 3; j++)
				cols[j] = html_tag(hdh, "<TD>", "</TD>", row);
		}

		if (mw_web_config.ff) {
			if (strcmp(mw_web_config.ff->name, ff->name) == 0)
				cols[i] = html_tag(hdh, "<B>", "</B>", cols[i]);
		}
		memcpy(&cfg, &mw_web_config, sizeof(struct config));
		cfg.mesg = -1;
		cfg.ff = ff;
		cols[i] = link_mw(hdh, &cfg, ff->topic, cols[i]);
		html_text(hdh, ff->name, cols[i]);
		if (++i == 3) {
			i = 0;
			row = NULL;
		}
	}

	if (i == 1)
		html_text(hdh, " ", cols[i++]);
	if (i == 2)
		html_text(hdh, " ", cols[i++]);
}

static struct html_object *page_skeleton(struct html_doc_head *hdh, char *title)
{
	struct html_object *html, *head, *body;

	html = html_tag(hdh, "<HTML>", "</HTML>", NULL);
	head = html_tag(hdh, "<HEAD>", "</HEAD>", html);
	head = html_tag(hdh, "<TITLE>", "</TITLE>", head);
	html_tag(hdh, title, NULL, head);
	body = html_tag(hdh, "<BODY BGCOLOR=\"#000000\">", "</BODY>", html);
	body = html_tag(hdh, "<TABLE COLS=1 BORDER=0 CELLPADDING=10 CELLSPACING=0 BGCOLOR=\"#CCDDFF\">", "</TABLE>", body);
	body = html_tag(hdh, "<TR>", "</TR>", body);
	body = html_tag(hdh, "<TD>", "</TD>", body);
	body = html_tag(hdh, "<TABLE COLS=1 WIDTH=\"99%\" HEIGHT=\"99%\">", "</TABLE>", body);
	body = html_tag(hdh, "<TR>", "</TR>", body);
	body = html_tag(hdh, "<TD>", "</TD>", body);

	return body;
}

static void page_header(struct html_doc_head *hdh, struct html_object *obj)
{
	struct html_object *ent;

	obj = html_tag(hdh, "<TABLE COLS=2 WIDTH=\"99%\" HEIGHT=\"100%\" BORDER=0 CELLSPACING=10 CELLPADDING=0>", "</TABLE>", obj);
	obj = html_tag(hdh, "<TR VALIGN=\"TOP\">", "</TR>", obj);
	ent = html_tag(hdh, "<TD WIDTH=\"59%\">", "</TD>", obj);
	box_options(hdh, ent);
	ent = html_tag(hdh, "<TD WIDTH=\"40%\">", "</TD>", obj);
	box_folders(hdh, ent);
}

static void page_contents(struct html_doc_head *hdh, struct html_object *obj)
{
	struct html_object *msg;
	struct Header hh, *hh2 = NULL;
	struct folder *ff2;
	struct folder *ff = mw_web_config.ff;
	struct person *pp = mw_web_config.pp;
	int n = 0, i = mw_web_config.ff->last;
	int err;
	int f = 1;

	obj = html_tag(hdh, "<TABLE COLS=1 ALIGN=\"CENTER\" WIDTH=\"95%\" BORDER=0 CELLSPACING=10 CELLPADDING=0>", "</TABLE>", obj);

	if (strcmp(ff->name, "NEW") == 0) {
		while((ff2 = folder_by_number(f++)) != NULL) {
			if (!f_active(ff2->status) | !allowed_r(ff2, pp))
				continue;
			message_iterate(ff2, pp, by_date, NULL, NULL);
		}

		while((hh2 = date_order_header(&ff2, hh2)) != NULL) {
			msg = html_tag(hdh, "<TR>", "</TR>", obj);
			msg = html_tag(hdh, "<TD>", "</TD>", msg);
			msg = html_tag(hdh, "<P>", "</P>", msg);
			box_message(hdh, ff2, hh2, msg);
		}
		return;
	}

	if (mw_web_config.mesg > 0) {
		err = message_by_number(ff, pp, &hh, mw_web_config.mesg);
		if (is_private(ff, pp) && strcmp(pp->name, hh.to) && !u_god(pp->status))
			err = EPERM;

		msg = html_tag(hdh, "<TR>", "</TR>", obj);
		msg = html_tag(hdh, "<TD>", "</TD>", msg);
		msg = html_tag(hdh, "<P>", "</P>", msg);
		if (err >= 0)
			box_message(hdh, ff, &hh, msg);
		return;
	}

	while((i > 0) && (n < 10)) {
		if (message_by_number(ff, pp, &hh, i--) < 0)
			continue;
		if (is_private(ff, pp) && strcmp(pp->name, hh.to) && !u_god(pp->status))
			continue;

		msg = html_tag(hdh, "<TR>", "</TR>", obj);
		msg = html_tag(hdh, "<TD>", "</TD>", msg);
		msg = html_tag(hdh, "<P>", "</P>", msg);
		box_message(hdh, ff, &hh, msg);
		n++;
	}
}

static void page_footer(struct html_doc_head *hdh, struct html_object *obj)
{
	html_tag(hdh, "<BR>", NULL, obj);
	obj = html_tag(hdh, "<DIV ALIGN=CENTER>", "</DIV>", obj);
	obj = html_tag(hdh, "<ADDRESS>", "</ADDRESS>", obj);
	obj = html_tag(hdh, "<EM>", "</EM>", obj);
	obj = html_tag(hdh, "Milliways WWW Interface: &copy;1999 <A HREF=\"http://www.sucs.swan.ac.uk/~rohan/\">Steve Whitehouse</A>, parts taken from Milliways III &copy;1992 <A HREF=\"http://www.sucs.swan.ac.uk/~arthur/\">Justin Mitchell</A>", NULL, obj);
}

static void create_doc(struct html_doc_head *hdh)
{
	struct html_object *doc;

	doc = page_skeleton(hdh, "Milliways WWW Interface");

	page_header(hdh, doc);
	page_contents(hdh, doc);
	page_footer(hdh, doc);
}

static int passwd_match(struct person *pp, char *passwd)
{
        char salt[3], buf[PASSWDSIZE+1];
        int i;

        strncpy(salt, pp->passwd, 2);
        strncpy(buf, crypt(passwd, salt), PASSWDSIZE);

        if (u_ban(pp->status))
                return 0;

        if ((i = strcmp(passwd, pp->passwd)) != 0)
                mw_web_config.failed_passwd = 1;

        return i;
}


static struct person *verify_user(char *name, char *pass)
{
	struct person *pp;

	if (!name || !pass)
		return NULL;

	if ((pp = person_by_name(name)) == NULL)
		return NULL;

	if (passwd_match(pp, pass))
		return pp;

	return NULL;
}

static struct person *cookie_user(char *host)
{
	struct person *pp;
	char *name = persist_find("USER");
	char *hcheck = persist_find("HOST");

	if (host == NULL)
		return NULL;

	if (name == NULL)
		return NULL;

	if (strcmp(host, hcheck))
		return NULL;

	pp = person_by_name(name);

	return pp;
}

static void read_config(void)
{
	struct config *c = &mw_web_config;
	char *cookie;
	char *host;
	char *name, *pass;
	char *fol, *msg;

	memset(c, 0, sizeof(struct config));

	host = getenv("REMOTE_ADDR");
	cookie = cookie_find("MW_WEB_REF");

	if (cookie) {
		if (persist_init(cookie) < 0) {
			cookie = NULL;
			persist_remove("USER");
			persist_remove("HOST");
		}
	}

	name = cgi_find("user");
	pass = cgi_find("passwd");
	fol = cgi_find("fol");
	msg = cgi_find("msg");

	if (!c->pp)
		c->pp = verify_user(name, pass);

	if (!c->pp && !name)
		c->pp = cookie_user(host);

	if (c->pp)
		c->set_cookie = 1;

	if (!c->pp)
		c->pp = person_by_name("guest");

	if (!c->pp)
		c->pp = person_by_name(NULL);

	if (!c->ff && fol)
		c->ff = folder_by_name(fol);

	if (c->ff && (!f_active(c->ff->status) || !allowed_r(c->ff, c->pp)))
		c->ff = NULL;

	if (!c->ff)
		c->ff = folder_by_name("Notices");

	if (msg)
		c->mesg = atoi(msg);

	if (!valid_number(c->ff, c->mesg))
		c->mesg = -1;

	persist_add("USER", c->pp->name);
	if (host)
		persist_add("HOST", host);

	if (c->set_cookie) {
		if ((cookie = persist_sync("mw")) != NULL)
			printf("Set-Cookie: MW_WEB_REF=%s\n", cookie);
	}
}

int main(int argc, char *argv[])
{
	struct html_doc_head hdh;

	cgi_process();
	cookie_process();
	render_init(&hdh);
	db_init(1);
	read_config();

	printf("Content-Type: text/html\n\n");

	create_doc(&hdh);

	render(&hdh);
	render_done(&hdh);

	return 0;
}

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#include <socket.h>
#include <nonce.h>
#include <util.h>
#include <rooms.h>
#include <talker_privs.h>
#include <ipc.h>
#include <perms.h>
#include <special.h>

#include "servsock.h"
#include "replay.h"

#define STORE_SIZE 1000

static uint64_t serial = 0;

static ipc_message_t ** store = NULL;
static int store_next = 0;
static int store_len = 0;

/* look at the history and set the serial number
 * appropriately.
 */
void load_serial(void)
{
	serial = 1;
}

static int store_wrap(int index)
{
	while (index < 0) index += STORE_SIZE;
	while (index >= STORE_SIZE) index -= STORE_SIZE;
	return index;
}

/* store the message for later replay */
void store_message(ipc_message_t *msg)
{
	if (store == NULL) {
		/* create the store */
		store = calloc(STORE_SIZE, sizeof(ipc_message_t *));
		store_next = 0;
		store_len = 0;
	}

	/* only store info/message, not actions */
	if (msg->head.type <= 26 &&
	   !( msg->head.type == IPC_TEXT || msg->head.type == IPC_WIZ))
		return;

	if (store_len >= STORE_SIZE) {
		/* store is full, discard oldest,
		 * it will have wrapped, so store_next is the last one */
		ipcmsg_destroy(store[store_next]);
		store_len--;
		store[store_next] = NULL;
	}

	/* add to ref count so it wont get cleaned away yet
	 * insert it at the current location and bump pointers
	 */
	msg->refcount++;
	store[store_next] = msg;
	store_len++;
	store_next = store_wrap( store_next + 1 );
}

/* assign a unique serial number to each message
 * thus giving a definitive replay ordering
 * also attach timestamp for human friendliness
 */
void assign_serial( ipc_message_t *msg )
{
	if (serial == 0) load_serial();
	msg->head.serial = serial++;
	msg->head.when = time(NULL);
}

void replay(ipc_connection_t *conn, ipc_message_t *msg)
{
	/* unpack the command */
	json_t *cmd = json_init(msg);

	/* find a pointer to the start/oldest item in store */
	int idx = store_wrap( store_next - store_len );

	/* which type did they say */
	if (json_object_get(cmd, "serial")!=NULL) {
		/* everything after serial # */
		uint64_t want = json_getint(cmd, "serial");
		for (int i=idx;i!=store_next;i=store_wrap(i+1)) {
			if ( store[i] == NULL) continue;
			if ( store[i]->head.serial >= want) {
				idx = i;
				break;
			}
		}
		/* if it fails, you get everything
		 * as it maybe got reset whilst you were away */
	}else
	if (json_object_get(cmd, "since")!=NULL) {
		/* everything after {unixtime} */
		int64_t want = json_getint(cmd, "since");
		while (idx != store_next) {
			/* list will be in date order */
			if (store[idx]!=NULL &&
				store[idx]->head.when >= want) break;
			idx = store_wrap(idx+1);
		}
		/* if it fails you get nothing as there is
		 * nothing newer (larger) than the date you gave */
	}else
	if (json_object_get(cmd, "count")!=NULL) {
		int want = json_getint(cmd, "count");
		if (want > store_len) want = store_len;
		idx = store_wrap( store_next - want );
	} else {
		json_decref(cmd);
		send_error(conn, msg, "Invalid replay command");
		return;
	}
	json_decref(cmd);

	/* who are we doing this for */
	struct user user;
	if (fetch_user(&user, conn->user) != 0) {
		send_error(conn, msg, "Error cannot find your user record");
		return;
	}
	RoomInit(&user.room);
	LoadRoom(&user.room, user.record.room);

	/* now, go and replay those messages that are appropriate */
	for (;idx != store_next; idx = store_wrap( idx + 1 )) {
		if (store[idx] == NULL) continue;

		/* this will be a subset of what you see in process_msg() */

		if (store[idx]->head.type == IPC_SAYTOROOM) {
			json_t * j = json_init( store[idx] );
			if (j == NULL) continue;

			const char * exclude = json_getstring(j, "exclude");

			if (exclude != NULL && strcasecmp(exclude, user.record.name)==0) {
				/* thats us ! shhh... */
				json_decref(j);
				continue;
			}
			json_decref(j);

			if (user.record.room == store[idx]->head.dst) {
				/* right room, send it */
				msg_attach(store[idx], conn);
			} else
			if (user.room.sproof < 1 && (cm_test(&user, CM_GLOBAL))) {
			/* room not soundproof, and user has global on */
				msg_attach(store[idx], conn);
			}
			/* couldnt have been for us */
			continue;
		}
		if (store[idx]->head.type == IPC_SAYTOUSER) {
			json_t * j = json_init( store[idx] );
			if (j == NULL) continue;

			const char * target = json_getstring(j, "target");

			if (target!=NULL && strcasecmp(target, user.record.name)==0) {
				/* yes, its for us */
				msg_attach(store[idx], conn);
			}
			json_decref(j);
			continue;
		}
		if (store[idx]->head.type == IPC_WIZ) {
			if (s_wizchat(&user)) {
				/* we are a wiz, we see this */
				msg_attach(store[idx], conn);
			}
			continue;
		}

		/* send them everything else */
		if (store[idx]->head.dst)
		msg_attach(store[idx], conn);
	}

	RoomDestroy(&user.room);

	return;
}

#include <stdlib.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include "bb.h"
#include "str_util.h"

#define min(a,b) a<b?a:b
#define max(a,b) a>b?a:b

int stringcmp(const char *a, const char *b, int n)
{
	int length;

	/* total match */
	if (n==-1)
	{
		length=max(strlen(a),strlen(b));
	}
	/* first partial match */
	else if (n==-2)
	{
		length=min(strlen(a),strlen(b));
	}
	/* match by atleast n (or shortest string) */
	else
	{
		if (strlen(a)<n) return 0;
		length=strlen(a)>strlen(b)?strlen(a):strlen(b);
		length=n<length?n:length;
	}

	if (length==0) return 0;
	return (!strncasecmp(a,b,length));
}

void strip_str(char *string)
{
	char *array;
	int i,ptr=0;
	int len;

	len=strlen(string);
	array=(char *)malloc(len+1);
	for (i=0;i<len;i++)
	{
		if (string[i]==033 || (string[i]>=040 && string[i]<=0176) || (unsigned char)string[i]>0177)
			array[ptr++]=string[i];
	}
	array[ptr]=0;
	strcpy(string,array);
	free(array);
}

int get_rand(int min, int max)
{
	int r = rand();
	float	fValue;

	fValue = (float)r * (float)(max - min + 1);
	fValue /= (float)RAND_MAX;
	fValue += (float)min;

	return ((int)fValue);
}

void string_add(char **str, const char *fmt, ...)
{
	va_list va;
	char *text = NULL;
	if (fmt == NULL) return;

	va_start(va, fmt);
	vasprintf(&text, fmt, va);
	va_end(va);

	if (*str == NULL)
	{
		/* was an empty string, give it this one */
		*str = text;
	} else {
		/* was not empty, append */
		*str = realloc(*str, sizeof(char) * (strlen(*str) + strlen(text) + 1));
		strcat(*str, text);
		free(text);
	}
}

int allspace(char *in)
{
	if (!strcmp(in, "")) {
		return(1);
	} else {
		int i;

		for(i=0;i<strlen(in);i++)
		{
			if (in[i]!=' ') return(0);
		}
		return(1);
	}
}

void strlower(char *szString)
{
	int	nLength, nIndex;

	nLength = strlen(szString);
	for (nIndex = 0; nIndex < nLength; nIndex++)
	{
		szString[nIndex] = tolower(szString[nIndex]);
	}
}

/* replace ESC characters ('\033') with "[ESC]", maybe in black on white */
static void strip_esc(char *const in, int hilite)
{
    static char szOutText[MAXTEXTLENGTH];
    int         oldlen = strlen(in), nInPos, nOutPos;
    if (oldlen >= MAXTEXTLENGTH-1) oldlen = MAXTEXTLENGTH;

    for (nInPos = 0, nOutPos = 0; nInPos < oldlen && nOutPos < MAXTEXTLENGTH-1; nInPos++)
    {
        /* escape character found */
        if (in[nInPos] == 033)
        {
            const char*const esc = "\033[7m[ESC]\033[0m";
            const char*const noesc = "[ESC]";
            if (hilite) {
                /* like strncpy, but avoid writing incomplete escape sequences */
                if (nOutPos >= (MAXTEXTLENGTH << 1) + strlen(esc)) {
                    break; /* not enough space left */
                }
                strcpy(szOutText+nOutPos, esc);
                nOutPos += strlen(esc);
            } else {
                strncpy(szOutText+nOutPos, noesc, MAXTEXTLENGTH-nOutPos-1);
                nOutPos += strlen(noesc);
            }
        }
        /* normal character */
        else
        {
                szOutText[nOutPos++] = in[nInPos];
        }
    }
    szOutText[nOutPos] = 0;
    strcpy(in, szOutText);
}

void escprintf(const char *szFormat, ...) __attribute__((format (printf, 1, 2)));
void escprintf(const char *szFormat, ...)
{
	static char	szText[MAXTEXTLENGTH];
	va_list		vaList;

	/* get text to process */
	va_start(vaList, szFormat);
	vsnprintf(szText, MAXTEXTLENGTH-1, szFormat, vaList);
	va_end(vaList);

	/* process control characters into strings */
	strip_esc(szText,1);

	printf("%s", szText);
}

/* returns a new string without any mw colour codes in */
char *strip_colours(const char *text)
{
	char *new;
	int i, j, len, k;

	len = strlen(text);
	new = malloc(sizeof(char) * (len+1));
	i=0;
	j=0;
	while (i<len)
	{
		if (text[i]==033)
		{
			i++;
			k=0;
			//utf8 safe way to skip the next two chars
			//we only need this in case someone is mean enough to try using something like
			// [ESC]r£ as a colour sequence which would previously have us remove the first
			// byte of the utf8 sequence leaving us with an invalid char.
			while(i<len && k<2)
			{
				if(((unsigned char)text[i] & 192)==192 )
				{
					// skip char is the first byte of a utf8 sequence
					i++;
					while( ((unsigned char)text[i] & 192) == 128 && i<len)
					{
						//skip all utf8 continuation bytes
						i++;
					}
				}
				else
				{
					// char was ascii so we can just skip it.
					i++;
				}
				k++;
			}

			continue;
		}
		new[j++]=text[i++];
	}
	new[j]=0;
	return(new);
}

char *quotetext(const char *a)
{
        int     count;
        int     ai, bi;
        char    *b;

        ai=0;
        count=0;
        while(a[ai]!=0)
        {
                if (a[ai]=='|') count++;
                ai++;
        }

        b = malloc(sizeof(char) * (strlen(a) + count + 1));

        ai=bi=0;
        while (a[ai]!=0)
        {
                if (a[ai]=='|')
                {
                        b[bi]=1;
                        bi++;
                }

                b[bi]=a[ai];
                bi++;
                ai++;
        }
        b[bi]=0;

        return (b);
}


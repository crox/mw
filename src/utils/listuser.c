#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <user.h>

int main(int argc, char **argv)
{
	struct person user;
	const char *path = "users.bb";
	int count=0;
	int ff;

	if (argc > 1)
		path = argv[1];

	if ((ff=open(path, O_RDONLY))<0) {
		fprintf(stderr, "%s: %s\n", path, strerror(errno));
		exit(1);
	}

	while (read(ff, &user, sizeof(user))>0) {
		printf("%d: %20s %40s\n", count, user.name, user.realname);
		count++;
	}
	close(ff);
}

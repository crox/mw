#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <jansson.h>

#include "mwcfg.h"

json_t *cfg;

static int cfg_new_opt_int(const struct cfg_default_opt *o)
{
	json_t *val = json_integer(o->o_val.integer);
	return json_object_set_new(cfg, o->o_name, val);
}

static int cfg_new_opt_bool(const struct cfg_default_opt *o)
{
	json_t *val = o->o_val.boolean ? json_true() : json_false();
	return json_object_set_new(cfg, o->o_name, val);
}

static int cfg_new_opt_str(const struct cfg_default_opt *o)
{
	json_t *val = json_string(o->o_val.string);
	return json_object_set_new(cfg, o->o_name, val);
}

typedef int (*cfg_new_opt_fn)(const struct cfg_default_opt *);

static const cfg_new_opt_fn cfg_new_opt_fns[] = {
	[CFG_OPT_INT] = cfg_new_opt_int,
	[CFG_OPT_BOOL] = cfg_new_opt_bool,
	[CFG_OPT_STR] = cfg_new_opt_str
};

int cfg_init_defaults(const struct cfg_default_opt *dft_opts)
{
	const struct cfg_default_opt *dopt;
	int ret;

	cfg = json_object();
	if (cfg == NULL) {
		perror("Failed to create default config object");
		return 1;
	}
	for (dopt = dft_opts; dopt && dopt->o_type != CFG_OPT_NULL; dopt++) {
		cfg_new_opt_fn fn;

		fn = cfg_new_opt_fns[dopt->o_type];
		if (fn == NULL) {
			fprintf(stderr, "Invalid config option type: %d\n", dopt->o_type);
			goto err_decref;
		}
		ret = fn(dopt);
		if (ret != 0) {
			fprintf(stderr, "Failed to build default config items\n");
			goto err_decref;
		}
	}
	return 0;
err_decref:
	json_decref(cfg);
	return 1;
}

static const char *jsontype_to_str[] = {
	[JSON_OBJECT]  = "Object",
	[JSON_ARRAY]   = "Array",
	[JSON_STRING]  = "String",
	[JSON_INTEGER] = "Integer",
	[JSON_REAL]    = "Real",
	[JSON_TRUE]    = "Boolean",
	[JSON_FALSE]   = "Boolean",
	[JSON_NULL]    = "Null"
};

static int cfg_obj_validate(json_t *obj)
{
	const char *key;
	json_t *cfg_val;
	int ret = 0;
	json_t *val;

	json_object_foreach(obj, key, val) {
		int usr_type, cfg_type;

		cfg_val = json_object_get(cfg, key);
		if (cfg_val == NULL)
			continue;
		usr_type = json_typeof(val);
		cfg_type = json_typeof(cfg_val);

		if (usr_type == JSON_FALSE) usr_type = JSON_TRUE;
		if (cfg_type == JSON_FALSE) cfg_type = JSON_TRUE;

		if (usr_type != cfg_type) {
			fprintf(stderr, "Wrong type for config option '%s'. ", key);
			fprintf(stderr, "Expected %s, got %s.\n",
			        jsontype_to_str[cfg_type], jsontype_to_str[usr_type]);
			ret = 1;
		}
	}
	return ret;
}

static int cfg_obj_merge(json_t *obj)
{
	const char *key;
	json_t *cfg_val;
	int ret = 0;
	json_t *val;

	json_object_foreach(obj, key, val) {
		cfg_val = json_object_get(cfg, key);
		if (cfg_val == NULL) {
			fprintf(stderr, "Unrecognised config option '%s'. Ignoring.\n", key);
			continue;
		}
		ret = json_object_set(cfg, key, val);
		if (ret != 0) {
			fprintf(stderr, "Failed to update config option '%s'. Giving up. ", key);
			break;
		}
	}
	return ret;
}

/**
 * Returns:
 * >0 if the path is not found or is not accessible
 * <0 if another error occurs
 *  0 if the path was successfully loaded
 **/
int cfg_load(const char *path)
{
	json_error_t err;
	json_t *obj;
	FILE *fp;
	int ret;
	int fd;

	fd = open(path, O_RDONLY);
	if (fd < 0) {
		if ((errno == EACCES || errno == ENOENT))
			return 1;
		fprintf(stderr, "Failed to open config file '%s': %s\n", path, strerror(errno));
		return -1;
	}
	fp = fdopen(fd, "r");
	if (fp == NULL) {
		fprintf(stderr, "Failed to create file stream for '%s': %s\n", path, strerror(errno));
		close(fd);
		return -1;
	}
	obj = json_loadf(fp, 0, &err);
	if (obj == NULL) {
		fprintf(stderr, "Failed to read config file '%s': %s\n", path, err.text);
		fclose(fp);
		return -1;
	}
	fclose(fp);
	ret = cfg_obj_validate(obj);
	if (ret != 0)
		return -1;
	printf("Loading configuration from '%s'\n", path);
	ret = cfg_obj_merge(obj);
	json_decref(obj);
	if (ret != 0)
		return -1;
	return 0;
}

void cfg_dump(FILE *f)
{
	if (json_dumpf(cfg, f, JSON_PRESERVE_ORDER|JSON_INDENT(1)))
		perror("Error encountered while printing config");
}

static json_t *cfg_get(const char *key)
{
	json_t *j = json_object_get(cfg, key);
	if (j == NULL)
		fprintf(stderr, "Invalid config option: %s\n", key);
	return j;
}

int cfg_get_bool(const char *key)
{
	json_t *j = cfg_get(key);
	if (j != NULL && !json_is_boolean(j)) {
		fprintf(stderr, "Config option is not a boolean: %s\n", key);
		return 0;
	}
	return j == json_true();
}

long cfg_get_int(const char *key)
{
	json_t *j = cfg_get(key);
	if (j != NULL && !json_is_integer(j)) {
		fprintf(stderr, "Config option is not an integer: %s\n", key);
		return 0;
	}
	return json_integer_value(j);
}

const char *cfg_get_string(const char *key)
{
	json_t *j = cfg_get(key);
	if (j != NULL && !json_is_string(j)) {
		fprintf(stderr, "Config option is not a string: %s\n", key);
		return 0;
	}
	return json_string_value(j);
}

int cfg_set_bool(const char *key, int bool_val)
{
	json_t *j = cfg_get(key);
	if (j == NULL)
		return 1;
	if (!json_is_boolean(j)) {
		fprintf(stderr, "Config option %s is not a boolean\n", key);
		return 1;
	}
	if (json_object_set(cfg, key, bool_val ? json_true() : json_false())) {
		fprintf(stderr, "Failed to set boolean config option %s to %s\n",
		        key, bool_val ? "true" : "false");
		return 1;
	}
	return 0;
}

int cfg_set_int(const char *key, long int_val)
{
	json_t *j = cfg_get(key);
	if (j == NULL)
		return 1;
	if (!json_is_integer(j)) {
		fprintf(stderr, "Config option %s is not an integer\n", key);
		return 1;
	}
	if (json_integer_set(j, int_val)) {
		fprintf(stderr, "Failed to set integer config option %s to %ld\n",
		        key, int_val);
		return 1;
	}
	return 0;
}

int cfg_set_string(const char *key, const char *str_val)
{
	json_t *j = cfg_get(key);
	if (j == NULL)
		return 1;
	if (!json_is_string(j)) {
		fprintf(stderr, "Config option %s is not a string\n", key);
		return 1;
	}
	if (json_string_set(j, str_val)) {
		fprintf(stderr, "Failed to set string config option %s to %s\n",
		        key, str_val);
		return 1;
	}
	return 0;
}

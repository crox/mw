#include <fcntl.h>
#include <regex.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <readline/readline.h>
#include <sys/time.h>
#include "script_inst.h"
#include "bb.h"
#include "incoming.h"
#include "talker_privs.h"
#include "talker.h"
#include "special.h"
#include "command.h"
#include "chattable.h"
#include "alarm.h"
#include "rooms.h"
#include "alias.h"
#include "str_util.h"
#include "expand.h"
#include "ipc.h"
#include "iconv.h"
#include "perms.h"
#include "main.h"
#include "user.h"
#include "mesg.h"
#include "util.h"
#include "who.h"

extern struct user * const user;
/* current script runaway variable */
extern long runaway;

Instruction inst_table[]={
{ "abs",	0x0000, 2, "Numerical absolute", scr_math2 },
{ "add",	0x0000, 2, "Numerical add", scr_math },
{ "and",	0x0000, 2, "Bitwise AND", scr_math },
{ "beep",	0x0000, 0, "Makes a beep on your console", scr_beep },
{ "boardexec",	0x0800, -1, "Feed command to bulletin board parser", scr_exec },
{ "body",	0x0000, 1, "Strip timestamps from event text", scr_user },
{ "boolopt",	0x0000, 1, "Check if given input is on/1/true etc..", scr_boolopt },
{ "call",	0x0000, 1, "Call a function and store its result in a variable", scr_jump },
{ "clearvars",	0x0000, 1, "Destroy variable(s) matching argument mask (or * for all)", scr_clearvars },
{ "date",	0x0000, 1, "Gets system date/time in dd/mm/yyyy HH:MM:SS format", scr_time },
{ "div",	0x0000, 2, "Numerical divide", scr_math },
{ "emote",	0x0000, 1, "Output emote text to talker", scr_say },
{ "end",	0x0000, 1, "End script processing", scr_end },
{ "eq",		0x0000, 2, "Case insensitive equivalence comparison check (==)", scr_compare },
{ "exec",	0x0000, -1, "Feed command to talker parser", scr_exec },
{ "ge",		0x0000, 2, "Greater than or equals comparison check (>=)", scr_compare },
{ "getvar",	0x0000, 2, "Set variable ??? to variable ???", scr_getvar },
{ "gt",		0x0000, 2, "Greater than comparison check (>)", scr_compare },
{ "idletime",	0x0000, 1, "Gets your current idle time in seconds", scr_time },
{ "ifall",	0x0000, 1, "If all comparisons passed JUMP to function ???", scr_jump },
{ "ifany",	0x0000, 1, "If any comparisons passed JUMP to function ???", scr_jump },
{ "ifclear",	0x0000, 0, "Clears the comparison tally", scr_jump },
{ "iffail",	0x0000, 1, "If any comparisons failed JUMP to function ???", scr_jump },
{ "ifnone",	0x0000, 1, "If all comparisons failed JUMP to function ???", scr_jump },
{ "inlist",	0x0000, 2, "Check if arg1 is equal to another argument", scr_inlist },
{ "iinlist",	0x0000, 2, "Check if arg1 is equal to another arg (ignore case)", scr_inlist },
{ "input",	0x0000, 2, "Accepts one line of input", scr_input },
{ "iregex",	0x0000, 2, "Case insensitive substring regex match", scr_regex },
{ "ised",	0x0000, 3, "Case insensitive string pattern replace", scr_regex },
{ "isedall",	0x0000, 3, "Case insensitive string pattern replace all occurrences", scr_regex },
{ "ison",	0x0000, 1, "Check if a user is on the talker", scr_ison },
{ "isadec",	0x0000, 1, "Check if given text is a valid decimal number", scr_isanum },
{ "isanum",	0x0000, 1, "Check if given text is a valid number", scr_isanum },
{ "jump",	0x0000, 1, "GOTO a label or GOSUB a function", scr_jump },
{ "le",		0x0000, 2, "Less than or equals comparison check (<=)", scr_compare },
{ "listvars",	0x0000, 1, "Get the variable list (with optional argument)", scr_listvars },
{ "local",	0x0000, -1, "Declare local variables", scr_local },
{ "logoninfo",	0x0000, 1, "Finds out how the user accessed the talker", scr_logoninfo },
{ "lt",		0x0000, 2, "Less than comparison check (<)", scr_compare },
{ "makestr",	0x0000, 3, "Create a string of multiple identical characters", scr_makestr },
{ "mod",	0x0000, 2, "Numerical modulus", scr_math },
{ "mul",	0x0000, 2, "Numerical multiply", scr_math },
{ "ne",		0x0000, 2, "Not equals comparison check (!=)", scr_compare },
{ "nprint",	0x0000, 1, "Print text to screen without newline appended", scr_print },
{ "or",		0x0000, 2, "Bitwise OR", scr_math },
{ "output",	0x0000, 1, "Set event output ON or OFF", scr_output },
{ "print",	0x0000, 1, "Print text to screen with newline appended", scr_print },
{ "q-room",	0x2000, 1, "Quietly change to this room number", scr_room },
{ "quit",	0x0000, 0, "Quits the board", scr_quit },
{ "rand",	0x0000, 3, "Random number generator", scr_rand },
{ "range",	0x0000, 3, "Check if arg1 is in the range [arg2..arg3]", scr_range },
{ "raw",	0x0001, 1, "Output raw text to the talker", scr_say },
{ "regex",	0x0000, 2, "Substring regex match", scr_regex },
{ "return",	0x0000, 1, "Exit from this function", scr_return },
{ "room",	0x0000, 1, "Change to this room number", scr_room },
{ "say",	0x0000, 1, "Output text to the talker", scr_say },
{ "sayto",	0x0001, 2, "Output text to the given person", scr_sayto },
{ "screxec",	0x0000, -1, "Feed command to script parser", scr_exec },
{ "sed",	0x0000, 3, "String pattern replace", scr_regex },
{ "sedall",	0x0000, 3, "String pattern replace all occurrences of pattern", scr_regex },
{ "sendipb",	0x0000, 1, "Send an IPC broadcast to all users", scr_sendipc },
{ "sendipc",	0x0000, 2, "Send an IPC message to another user", scr_sendipc },
{ "sendrpb",	0x0000, 2, "Send an RPC typed broadcast to a user", scr_sendrpc },
{ "sendrpc",	0x0000, 3, "Send an RPC typed message to another user", scr_sendrpc },
{ "set",	0x0000, 2, "Set variable XXX to YYY", scr_set },
{ "shout",	0x0000, 1, "Shout text to the talker", scr_say },
{ "showrunaway",0x2000, 0, "Prints current runaway value to screen", scr_showrunaway },
{ "sleep",	0x0000, 1, "Pauses execution for ??? 1/10th seconds", scr_sleep },
{ "split",	0x0000, 3, "Splits string into first word, and rest of string", scr_split },
{ "strcat",	0x0000, 2, "Append one string to another", scr_strcat },
{ "strchr",	0x0000, 3, "Get index of first char in a string", scr_string },
{ "stristr",    0x0000, 3, "Get index of first substring in a string (no case)", scr_string },
{ "strlen",	0x0000, 2, "Get length of a string", scr_string },
{ "strmid",	0x0000, 3, "Gets a subsection XXX-YYY of a string", scr_string },
{ "strnchr",	0x0000, 3, "Get index of first char NOT in a string", scr_string },
{ "strstr",	0x0000, 3, "Get index of first substring in a string", scr_string },
{ "sub",	0x0000, 2, "Numerical subtract", scr_math },
{ "talkpriv",	0x0000, 3, "Gets a string of current script effecting privs", scr_talkpriv },
{ "toascii",	0x0000, 2, "Gets the ascii value of a character", scr_base },
{ "tolower",	0x0000, 2, "Gets lowercase version of a string", scr_case },
{ "toupper",	0x0000, 2, "Gets uppercase version of a string", scr_case },
{ "tovalue",	0x0000, 2, "Gets the character represented by an ascii value", scr_base },
{ "unixtime",	0x0000, 1, "Gets the current unix time", scr_time },
{ "unset",	0x0000, 1, "Destroy a variable", scr_unset },
{ "user",	0x0000, 1, "Gets the user that caused the current event", scr_user },
{ "useridle",	0x0000, 2, "Gets the idle time in seconds of the specified user", scr_time },
{ "userroom",	0x0000, 2, "Gets the room number of the specified user", scr_roomnum },
{ "version",	0x0000, 1, "Gets the milliways version string", scr_version },
{ "whenami",	0x0000, 1, "Gets the current timestamp (HH:MM)", scr_time },
{ "whereami",	0x0000, 1, "Gets your room number", scr_roomnum },
{ "whoami",	0x0000, 1, "Gets your username", scr_user },
{ "wholist",	0x0000, 1, "Gets a list of users currently on the talker", scr_wholist },
{ "xor",	0x0000, 2, "Bitwise XOR", scr_math },
{ NULL,		0x0000, 0, NULL, NULL }
};

/********************************
 *** Instruction set for scripts
 ********************************/

void scr_time( struct code *pc, int fargc, char **fargv )
{
	_autofree char *what = NULL;
	_autofree char *value = NULL;
	time_t now = time(0);

	if (pc->argc < 1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what=eval_arg(pc->argv[0], fargc, fargv);

	if (!strcasecmp(pc->inst->name, "unixtime"))
	{
		string_add(&value, "%ld", now);
	}
	else if (!strcasecmp(pc->inst->name, "idletime"))
	{
		string_add(&value, "%ld", now - user->record.idletime);
		if (script_debug) printf("- Idle time is %ld\n", now - user->record.idletime);
	}
	else if (!strcasecmp(pc->inst->name, "useridle"))
	{
		_autofree char *uname = eval_arg(pc->argv[1], fargc, fargv);

		if (script_debug) escprintf("- %s: Getting idletime of user '%s'.\n", pc->inst->name, uname);

		json_t * wlist = grab_wholist();
		if (wlist == NULL)
		{
			/* hideous problems with user info files */
			printf("- %s: Can't open user/who information files\n", pc->inst->name);
			return;
		}

		/* set default idle time to zero */
		string_add(&value, "0");

		size_t wi;
		json_t *entry;
		json_array_foreach(wlist, wi, entry) {
			const char *name = json_getstring(entry, "name");
			time_t idletime = json_getint(entry, "idletime");

			if (!strcasecmp(name, uname))
			{
				string_add(&value, "%ld", now - idletime);
				if (script_debug) printf("- %s: Idletime is %ld\n", pc->inst->name, now - idletime);
			}
		}
		json_decref(wlist);
	}
	else if (!strcasecmp(pc->inst->name, "whenami"))
	{
		struct tm *tt;
		
		tt=localtime(&now);
		value=malloc(10);
		strftime(value, 9, "%H:%M", tt);
	}
	else if (!strcasecmp(pc->inst->name, "date"))
	{
		time_t		oldt;
		struct tm	*t;

		/* get current time, and convert it into a local information structure */
		time(&oldt);
		t = localtime(&oldt);

		/* print date information into the string in the format "dd/mm/yyyy HH:MM:SS" */
		string_add(&value, "%02d/%02d/%04d %02d:%02d:%02d", t->tm_mday, t->tm_mon + 1, t->tm_year + 1900, t->tm_hour, t->tm_min, t->tm_sec);
	}
	var_str_force_2(what, value);
}

void scr_say( struct code *pc, int fargc, char **fargv )
{
	int i, size, count;
	_autofree char *tmp = NULL;
	_autofree char *p = NULL;
	char utf8buff[MAXTEXTLENGTH];
	int conversion_result;

	if (pc->argc==0) return;
	if (!cm_test_any(user, CM_ONCHAT)) return;

	size=0;
	for (i=0; i<pc->argc; i++)
		size+=strlen(pc->argv[i])+1;

	/* overly generous allocation as later code isnt clean. */
	size+=512;
	tmp=(char *)malloc(size);
	*tmp=0;

	count=0;
	for (i=0; i<pc->argc; i++)
	{
		if (i>0)  {strcat(tmp, " "); count++; }
		p=eval_arg(pc->argv[i], fargc, fargv);
		count+=strlen(p);
		if (count>=size) { size=count+512; tmp=realloc(tmp, size); }
		strcat(tmp, p);
	}

	flood++;
	if (flood > flood_limit)
	{
		printf("FLOOD: This script has flooded the room. Terminating.\n");
		script_terminate = 2;
		return;
	}

	conversion_result=convert_string_charset(tmp, "UTF-8", strlen(tmp), utf8buff, "UTF-8", MAXTEXTLENGTH-100, NULL, NULL, NULL, NULL, "%");
	if(conversion_result < 0)
	{
		printf("Error %d occured trying to clean up the script output.\n", conversion_result);
		return;
	}
	if(conversion_result & WINVALIDCHARS)
	{
		printf("Error: Your script produced invalid utf-8 which has not been sent.\n");
		return;
	}
	if(conversion_result & WOUTPUTTOOSHORT)
	{
		printf("Warning: Your script produced too much text which has been truncated.\n");
	}



	if (!strcasecmp(pc->inst->name, "say"))
	{
		chat_say(utf8buff);
	}
	else if (!strcasecmp(pc->inst->name, "shout"))
	{
		talk_send_shout(utf8buff);
	}
	else if (!strcasecmp(pc->inst->name, "emote"))
	{
		talk_send_emote(utf8buff, user->record.room, 0);
	}
	else if (!strcasecmp(pc->inst->name, "raw"))
	{
		talk_send_raw(utf8buff, user->record.room);
	}
}

void scr_beep( struct code *pc, int fargc, char **fargv )
{
	printf("");
}

void scr_showrunaway( struct code *pc, int fargc, char **fargv )
{
	printf("%ld\n", runaway);
}

void scr_output( struct code *pc, int fargc, char **fargv )
{
	_autofree char *what = NULL;
	int i;

	if (event_user==NULL)
	{
		if (script_debug) printf("Error in %s at %s, not running in event.\n", pc->inst->name, pc->debug);
		return;
	}

	if (pc->argc != 1) {
		if (script_debug) printf("Error in %s at %s, incorrect number of arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what=eval_arg(pc->argv[0], fargc, fargv);

	if ((i=BoolOpt(what))==-1)
	{
		if (script_debug) printf("- OUTPUT: Error, invalid argument\n");
	}
	else script_output = i;
}

void scr_wholist( struct code *pc, int fargc, char **fargv )
{
	_autofree char *what = NULL;
	_autofree char *buff = NULL;

	if (pc->argc < 1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what = eval_arg(pc->argv[0], fargc, fargv);

	json_t * wlist = grab_wholist();
	if (wlist == NULL) return;

	size_t wi;
	json_t *entry;
	json_array_foreach(wlist, wi, entry) {
		json_t * perms = json_object_get(entry, "perms");
		const char *name = json_getstring(entry, "name");
		const char *chatmode = NULL;
		if (perms!=NULL) chatmode=json_getstring(perms, "chatmode");

		if (chatmode != NULL && strchr(chatmode, 'c')!=NULL)
		{
			string_add(&buff, "%s ", name);
		}
	}
	json_decref(wlist);

	{
		char *p;
		if ((p=strrchr(buff,' '))!=NULL) *p=0;
	}

	var_str_force_2(what, buff);
}

void scr_roomnum( struct code *pc, int fargc, char **fargv )
{
	_autofree char *what = NULL;
	char rn[10];

	/* set initial room to -1 for unknown */
	rn[0] = '-';
	rn[1] = '1';
	rn[2] = 0;

	if (pc->argc < pc->inst->args) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what=eval_arg(pc->argv[0], fargc, fargv);

	if (!strcasecmp(pc->inst->name, "whereami"))
	{
		if (!cm_test(user, CM_ONCHAT))
		{
			snprintf(rn, 9, "-1");
		}
		else
		{
			/* if have the global script priv, then will always return your room number */
			if (cm_test(user, CP_CANGLOBAL))
			{
				snprintf(rn, 9, "%d", user->record.room);
			}
			/* if room is not hidden, or locked, then return room number */
			else if (user->room.hidden <= 0 && user->room.locked <= 0)
			{
				snprintf(rn, 9, "%d", user->record.room);
			}
			/* cant return room number */
			else
			{
				snprintf(rn, 9, "-1");
				if (script_debug) printf("- %s: cant find current room (lost)\n", pc->inst->name);
			}
		}
	}
	else if (!strcasecmp(pc->inst->name, "userroom"))
	{
		_autofree char *uname = NULL;
		int rnum = -1;

		uname=eval_arg(pc->argv[1], fargc, fargv);

		if (script_debug) escprintf("- %s: Getting room of user '%s'.\n", pc->inst->name, uname);

		json_t * wlist = grab_wholist();
		if (wlist == NULL) return;

		size_t wi;
		json_t *entry;
		json_array_foreach(wlist, wi, entry) {
			json_t * perms = json_object_get(entry, "perms");
			const char *name = json_getstring(entry, "name");
			const char *chatmode = NULL;
			if (perms!=NULL) chatmode=json_getstring(perms, "chatmode");
			int room = json_getint(entry, "channel");

			/* wrong person */
			if (strcasecmp(name, uname)!=0) continue;
			/* wasnt on chat */
			if (chatmode != NULL && strchr(chatmode, 'c')==NULL) continue;
			rnum = room;
		}
		json_decref(wlist);

		snprintf(rn, 9, "%d", rnum);
	}

	var_str_force_2(what, rn);
}

void scr_talkpriv( struct code *pc, int fargc, char **fargv )
{
	_autofree char *what = NULL;
	_autofree char *buff = NULL;
	unsigned long mask = 0xffffffff;

	if (pc->argc < 1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what = eval_arg(pc->argv[0], fargc, fargv);

	/* Empty string "" means no privs. For every extra priv/mode available, the following character is added:
	 *  T     - Timestamps enabled
	 *  F     - Force
	 *  B     - Beeps on
	 *  S     - Superuser
	 *  <a-z> - chatmodes
	 */

	mask &= ~(CM_ONCHAT);
	mask &= ~(CM_STICKY);
	if (s_timestamp(user)) string_add(&buff, "T");
	if (!u_beep(user)) string_add(&buff, "B");
	if (u_god(user)) string_add(&buff, "S");
	string_add(&buff, "%s", display_cmflags(user->record.chatmode & mask));

	var_str_force_2(what, buff);
}

void scr_quit( struct code *pc, int fargc, char **fargv )
{
	/* close down milliways normally */
	close_down(0, NULL, NULL);
}

void scr_exec( struct code *pc, int fargc, char **fargv )
{
	char *p;

	if (pc->argc<1) {
		if (script_debug) printf("Error in function '%s' at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	p=eval_arg(pc->argv[0], fargc, fargv);
	strip_str(p);

	if (!strcasecmp(pc->inst->name, "exec"))
	{
		if (cm_test(user, CM_ONCHAT)) DoCommand(p, chattable);
	}
	else if (!strcasecmp(pc->inst->name, "boardexec")) {
		set_rights();
		DoCommand(p, table);
		set_talk_rights();
	}
	else if (!strcasecmp(pc->inst->name, "screxec")) {
		if (!ExecInst(p))
		{
			if (script_debug) printf("Error in function '%s' at %s invalid instruction execution.\n", pc->inst->name, pc->debug);
		}
	}

	free(p);
}

void scr_print( struct code *pc, int fargc, char **fargv )
{
	int	i, size, len;
	char	*a, *text;

	/* no text to print, dont do anything */
	if (pc->argc < 1) return;

	/* this allows concatenation of arguments with a space between
	 * work out entire length of phrase
	 */
	size = 0;
	for (i = 0; i<pc->argc; i++) size += strlen(pc->argv[i]) + 1;

	/* to start with, we assume the text will expand to no more than double
	 * its size when variables are expanded out etc.
	 */
	size = size * 2;
	text = (char *)malloc(size);

	/* concatenate argument strings, and expand out */
	*text = 0;
	for (i=0; i < pc->argc; i++)
	{
		/* expand out current argument */
		a = eval_arg(pc->argv[i], fargc, fargv);
		/* find its length */
		len = strlen(a);
		/* if new text string length is greater than current length, reallocate string memory */
		if (strlen(text) + len + 5 >= size)
		{
			size += len + 80;
			text = realloc(text, size);
		}
		strcat(text, a);
		/* if not the beginning or end of string, add a space between words */
		if (i>0 && i<pc->argc-1) strcat(text, " ");
		free(a);
	}

	/* depending on what command was issued, write the string to screen with or without a newline on the end */
	if (!strcasecmp(pc->inst->name, "print")) {
		display_message(text, 0, 1);
	} else if (!strcasecmp(pc->inst->name, "nprint")) {
		display_message(text, 0, 0);
	}
	free(text);
}

int isanum(const char *a, int *result, int onlydecimal)
{
	char *end;
	if (a==NULL || strlen(a)<1) return(0);

	*result=strtol(a, &end, 10);
	if (*end==0)
	{
		return (1);
	}
	else if (!onlydecimal)
	{
		*result=strtol(a, &end, 16);
		if (*end==0)
		{
			return (1);
		}
		else
		{
			return (0);
		}
	}
	else
	{
		return (0);
	}
}

int isanumul(const char *a, unsigned long *result, int onlydecimal)
{
	char *end;
	if (a==NULL || strlen(a)<1) return(0);

	*result=strtoul(a, &end, 10);
	if (*end==0)
	{
		return (1);
	}
	else if (!onlydecimal)
	{
		*result=strtoul(a, &end, 16);
		if (*end==0)
		{
			return (1);
		}
		else
		{
			return (0);
		}
	}
	else
	{
		return (0);
	}
}

void scr_compare( struct code *pc, int fargc, char **fargv )
{
	_autofree char *a = NULL;
	_autofree char *b = NULL;
	int aa=0, bb=0;
	int numeric=0;

	if (pc->argc<2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	compare_count++;

	a=eval_arg(pc->argv[0], fargc, fargv);
	b=eval_arg(pc->argv[1], fargc, fargv);

	if (isanum(a, &aa, 0) && isanum(b, &bb, 0)) {
		numeric=1;
	}

	if (script_debug)
	{
		if (numeric) escprintf("- Compare numbers '%d' (%s) and '%d' (%s)\n",aa, a, bb, b);
		else escprintf("Compare strings '%s' and '%s'\n", a, b);
	}

	if (!strcasecmp(pc->inst->name, "eq"))
	{
		if (numeric) {
			if (aa==bb) compare_match++;
		} else {
			if (!strcasecmp(a,b)) compare_match++;
		}
	}else
	if (!strcasecmp(pc->inst->name, "ne"))
	{
		if (numeric) {
			if (aa!=bb) compare_match++;
		} else {
			if (strcasecmp(a,b)) compare_match++;
		}
	}else
	if (!strcasecmp(pc->inst->name, "lt"))
	{
		if (numeric) {
			if (aa<bb) compare_match++;
		} else {
			if (strcasecmp(a,b) <0) compare_match++;
		}
	}else
	if (!strcasecmp(pc->inst->name, "gt"))
	{
		if (numeric) {
			if (aa>bb) compare_match++;
		} else {
			if (strcasecmp(a,b) >0) compare_match++;
		}
	}else
	if (!strcasecmp(pc->inst->name, "le"))
	{
		if (numeric) {
			if (aa<=bb) compare_match++;
		} else {
			if (strcasecmp(a,b) <=0) compare_match++;
		}
	}else
	if (!strcasecmp(pc->inst->name, "ge"))
	{
		if (numeric) {
			if (aa>=bb) compare_match++;
		} else {
			if (strcasecmp(a,b) >=0) compare_match++;
		}
	}
}

void scr_math( struct code *pc, int fargc, char **fargv )
{
	int a, b;
	_autofree char *aa = NULL;
	_autofree char *bb = NULL;
	var_op_t var;
	int result=0;

	if (pc->argc<2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	aa=eval_arg(pc->argv[0], fargc, fargv);

	if (!eval_var(aa, &var))
	{
		scr_devel_msg(pc, "Invalid variable name '%s'", aa);
	}

	if (!VAR_FOUND(&var)) {
		VAR_INT_CREATE(&var, 0);
		if (script_debug) escprintf("- %s: creating global var $%s initialising to \"0\".\n", pc->inst->name, aa);
	}

	bb=eval_arg(pc->argv[1], fargc, fargv);

	if (var_isanum(&var, &a, 0) && isanum(bb, &b, 0)) {
		/* all hunky dory */
	}else
	{ /* looks like their not numbers, cant math strings */
		if (script_debug) escprintf("- math: either $%s (%s) or '%s' were not numbers\n", aa, var_str_val(&var), bb);
		VAR_KEY_FREE(&var);
		return;
	}

	if (script_debug) escprintf("- math operation on $%s (%d) and %d\n", aa, a, b);

	if (!strcasecmp(pc->inst->name, "add"))
		result = a+b;
	else
	if (!strcasecmp(pc->inst->name, "sub"))
		result = a-b;
	else
	if (!strcasecmp(pc->inst->name, "mul"))
		result = a*b;
	else
	if (!strcasecmp(pc->inst->name, "and"))
		result = a&b;
	else
	if (!strcasecmp(pc->inst->name, "or"))
		result = a|b;
	else
	if (!strcasecmp(pc->inst->name, "xor"))
		result = a^b;
	else
	if (!strcasecmp(pc->inst->name, "div")) {
		if (b==0) result=0; else
		result = a/b;
	} else
	if (!strcasecmp(pc->inst->name, "mod"))
	{
		if (b==0) result=a; else result = a%b;
	}

	VAR_INT_UPDATE(&var, result);
	VAR_KEY_FREE(&var);
}

void scr_math2( struct code *pc, int fargc, char **fargv )
{
	int a;
	_autofree char *aa = NULL;
	_autofree char *bb = NULL;
	var_op_t var;
	int result=0;

	if (pc->argc<2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	aa=eval_arg(pc->argv[0], fargc, fargv);

	if (!eval_var(aa, &var))
	{
		scr_devel_msg(pc, "Invalid variable name '%s'", aa);
	}

	if (!VAR_FOUND(&var)) {
		VAR_INT_CREATE(&var, 0);
		if (script_debug) escprintf("- %s: creating global var $%s initialising to \"0\".\n", pc->inst->name, aa);
	}

	bb=eval_arg(pc->argv[1], fargc, fargv);

	if (isanum(bb, &a, 0)) {
		/* all hunky dory */
	}else
	{
		/* looks like arg2 is not a number, cant math string */
		if (script_debug) escprintf("- math: '%s' is not a number\n", bb);
		VAR_KEY_FREE(&var);
		return;
	}

	if (script_debug) printf("- math operation on %d\n", a);

	if (!strcasecmp(pc->inst->name, "abs"))
	{
		if (a>0) result = a; else result = -a;
	}

	VAR_INT_UPDATE(&var, result);
	VAR_KEY_FREE(&var);
}

void scr_sleep( struct code *pc, int fargc, char **fargv )
{
	_autofree char *aa = NULL;
	int	a;
	struct timeval delay;

	if (pc->argc<1)
	{
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
	}

	aa=eval_arg(pc->argv[0], fargc, fargv);

	if (isanum(aa, &a, 0)) {
		/* delay for n 1/10th of a second */
		delay.tv_sec = a / 10;
		delay.tv_usec = (a % 10) * 100000;
		alarm_enable();
		while (timerisset(&delay) && !script_terminate) {
			alarm_sleep(&delay, 0);
		}
	}
	else
		if (script_debug) escprintf("- Sleep input not number '%s'\n", aa);
}

void scr_rand( struct code *pc, int fargc, char **fargv )
{
	char	buff[20];
	_autofree char *aa = NULL;
	_autofree char *bb = NULL;
	_autofree char *what = NULL;
	int	a, b;

	if (pc->argc<3) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);
	aa=eval_arg(pc->argv[1], fargc, fargv);
	bb=eval_arg(pc->argv[2], fargc, fargv);

	if (!isanum(aa, &a, 0) || !isanum(bb, &b, 0))
	{
		/* looks like they're not numbers, cant math strings */
		var_str_force_2(what, "-1");
		if (script_debug) escprintf("- rand: either '%s' or '%s' were not numbers\n", aa, bb);
		return;
	}

	/* bad min/max */
	if (b < a)
	{
		var_str_force_2(what, "-1");
		if (script_debug) printf("- rand operation. second argument (%d) smaller than first (%d)\n", b, a);
		return;
	}
	/* not positive */
	if ((a < 0) || (b < 0))
	{
		var_str_force_2(what, "-1");
		if (script_debug) printf("- rand operation. arguments (%d, %d) must be positive", a, b);
		return;
	}

	if (script_debug) printf("- rand operation on %d and %d into $%s\n", a, b, what);

	snprintf(buff, 19, "%d", get_rand(a,b));
	var_str_force_2(what, buff);
}

void scr_makestr( struct code *pc, int fargc, char **fargv )
{
	_autofree char *aa=NULL;
	_autofree char *bb=NULL;
	_autofree char *cc=NULL;
	_autofree char *out=NULL;
	var_op_t var;
	int i, j;

	if (pc->argc<3) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	/* makestr <output> <number> <char> */

	aa=eval_arg(pc->argv[0], fargc, fargv);
	bb=eval_arg(pc->argv[1], fargc, fargv);
	cc=eval_arg(pc->argv[2], fargc, fargv);

	VAR_STR_PREPARE_2(&var, local_vars, &var_list, aa, NULL);

	if (!isanum(bb, &i, 0)) {
		if (script_debug) escprintf("- %s: cant make string because arg2 '%s' is not a number\n", pc->inst->name, bb);
		return;
	}
	if (i < 1) {
		if (script_debug) printf("- %s: cant make string because arg2 '%d' is less than 1\n", pc->inst->name, i);
		return;
	}
	if (!strcmp(cc, "")) {
		if (script_debug) printf("- %s: cant make string because arg3 is empty\n", pc->inst->name);
		return;
	}

	for (j = 0; j < i; j++) {
		string_add(&out, "%s", cc);
	}

	VAR_STR_UPDATE(&var, out);

	if (script_debug) escprintf("- %s: '%d' x '%s' --> '%s'\n", pc->inst->name, i, cc, var_str_val(&var));
}

void scr_split( struct code *pc, int fargc, char **fargv )
{
	_autofree char *aa=NULL;
	_autofree char *bb=NULL;
	_autofree char *cc=NULL;
	_autofree char *text=NULL;
	var_op_t var1, var2;

	if (pc->argc<3) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	/* split <input> <head> <tail> */

	aa=eval_arg(pc->argv[0], fargc, fargv);
	bb=eval_arg(pc->argv[1], fargc, fargv);
	cc=eval_arg(pc->argv[2], fargc, fargv);

	VAR_STR_PREPARE_2(&var1, local_vars, &var_list, bb, NULL);
	VAR_STR_PREPARE_2(&var2, local_vars, &var_list, cc, NULL);

	/* source string was empty */
	if (!strcmp(aa, "")) {
		if (script_debug) printf("- Source string to %s empty, returning blank head and tail.\n", pc->inst->name);
		VAR_STR_UPDATE(&var1, "");
		VAR_STR_UPDATE(&var2, "");
		return;
	}

	/* skip spaces at start of string */
	text = strdup(aa);
	char *ccp = text;
	while ((ccp != NULL) && isspace(*ccp)) ccp++;

	/* if all spaces at start of string, then return the spaces as head, and tail as nothing */
	if (ccp == NULL)
	{
		VAR_STR_UPDATE(&var1, aa);
		VAR_STR_UPDATE(&var2, "");
	}
	/* if there were spaces at the front of the string, ignore them, and split about next space */
	else
	{
		char *bbp;
		if ((bbp = strchr(ccp, ' ')) == NULL)
		{
			VAR_STR_UPDATE(&var1, ccp);
			VAR_STR_UPDATE(&var2, "");
		}
		else
		{
			char *dd = bbp;
			dd++;
			VAR_STR_UPDATE(&var2, dd);
			*bbp=0;
			VAR_STR_UPDATE(&var1, ccp);
		}
	}

	if (script_debug) escprintf("- %s: '%s' --> <'%s','%s'>\n", pc->inst->name, aa, var_str_val(&var1), var_str_val(&var2));
}

void scr_strcat( struct code *pc, int fargc, char **fargv )
{
	var_op_t var;
	_autofree char *a=NULL;
	_autofree char *b=NULL;
	_autofree char *nbuff=NULL;

	if (pc->argc<2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	a=eval_arg(pc->argv[0], fargc, fargv);

	VAR_SEARCH_2(&var, local_vars, &var_list, a);

	b=eval_arg(pc->argv[1], fargc, fargv);

	if (VAR_FOUND(&var))
	{
		string_add(&nbuff, "%s", var_str_val(&var));
		string_add(&nbuff, "%s", b);
		VAR_STR_UPDATE(&var, nbuff);
	} else
	{
		VAR_STR_CREATE(&var, b);
	}
}

void scr_set(struct code *pc, int fargc, char **fargv)
{
	var_op_t var;
	_autofree char *what = NULL;
	_autofree char *text = NULL;

	if (pc->argc < 2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);
	if (!eval_var(what, &var))
	{
		scr_devel_msg(pc, "'%s' is not a valid variable name", what);
		free(what);
		return;
	}

	for (int i=1; i<pc->argc; i++) {
		char * a = eval_arg(pc->argv[i], fargc, fargv);
		string_add(&text, "%s", a);
		if (i>0 && i<pc->argc-1) string_add(&text, " ");
		free(a);
	}

	if (VAR_FOUND(&var)) {
		if (script_debug) escprintf("- set: updating var $%s (%s) to '%s'\n", what, var_str_val(&var), text);
		VAR_STR_UPDATE(&var, text);
	} else {
		VAR_STR_CREATE(&var, text);
		if (script_debug) escprintf("- set: creating var $%s setting to '%s'\n", what, text);
	}

	VAR_KEY_FREE(&var);
}

void scr_getvar(struct code *pc, int fargc, char **fargv)
{
	var_op_t var, var2;
	_autofree char *what = NULL;
	_autofree char *value=NULL;

	if (pc->argc < 2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);
	if (!eval_var(what, &var))
	{
		scr_devel_msg(pc, "'%s' is not a valid variable name", what);
		return;
	}

	value=eval_arg(pc->argv[1], fargc, fargv);

	if (eval_var(value, &var2))
	{
		if (VAR_FOUND(&var2))
		{
			free(value);
			value=strdup(var_str_val(&var2));
		} else
		{
			if (script_debug) escprintf("- getvar: cant find var '%s' to copy from. returning blank\n", value);
			free(value);
			value=strdup("");
		}
		VAR_KEY_FREE(&var2);
	} else
	{
		scr_devel_msg(pc, "var name '%s' is invalid. returning blank", value);
		free(value);
		value=strdup("");
	}

	if (VAR_FOUND(&var)) {
		if (script_debug) escprintf("- set: updating var $%s (%s) to '%s'\n", what, var_str_val(&var), value);
		VAR_STR_UPDATE(&var, value);
	} else {
		VAR_STR_CREATE(&var, value);
		if (script_debug) escprintf("- set: creating var $%s setting to '%s'\n", what, value);
	}
	VAR_KEY_FREE(&var);
}

void scr_unset(struct code *pc, int fargc, char **fargv)
{
	var_op_t var;
	_autofree char *what = NULL;

	if (pc->argc < 1)
	{
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);

	if (!eval_var(what, &var))
	{
		scr_devel_msg(pc, "'%s' is not a valid variable name", what);
	} else
	{
		if (VAR_FOUND(&var)) VAR_DESTROY(&var);
		else if (script_debug) escprintf("- unset: %s was not a variable.\n", what);
		VAR_KEY_FREE(&var);
	}
}

void scr_clearvars(struct code *pc, int fargc, char **fargv)
{
	var_op_t iter;
	var_op_t op;
	_autofree char *what = NULL;
	_autofree char *key = NULL;

	if (pc->argc > 1) {
		if (script_debug) printf("Error in %s at %s too many arguments.\n", pc->inst->name, pc->debug);
		return;
	} else if (pc->argc == 1) {
		what=eval_arg(pc->argv[0], fargc, fargv);
	}

	VAR_LIST_ITERATE(&iter, &var_list);
	while (VAR_FOUND(&iter))
	{
	 	key = strdup(VAR_STR_KEY(&iter));
		if (what == NULL || !strncasecmp(what, key, strlen(what)))
		{
			VAR_OP_INIT(&op, &var_list);
			VAR_SEARCH(&op, key);
			VAR_DESTROY(&op);
			VAR_LIST_ITERATE(&iter, &var_list); /* start again */
		} else
		{
			VAR_LIST_NEXT(&iter);
		}
	}

	/* 2nd pass garbage-collects hashes (does nothing with lists)
	 * Maybe we need a variant of VAR_FREELIST?
	 */
	VAR_LIST_ITERATE(&iter, &var_list);
	while (VAR_FOUND(&iter)) VAR_LIST_NEXT(&iter);
}

void scr_local(struct code *pc, int fargc, char **fargv )
{
	var_op_t var_op;
	char *name;
	int i;

	for (i=0; i<pc->argc; i++)
	{
		name = eval_arg(pc->argv[i], fargc, fargv);
		VAR_OP_INIT(&var_op, local_vars);
		VAR_SEARCH(&var_op, name);
		if (VAR_FOUND(&var_op))
		{
			if (script_debug) escprintf("- Local variable `%s' exists - ignoring\n", name);
		} else
		{
			if (script_debug) escprintf("- Creating local variable `%s', initialising to ''\n", name);
			VAR_STR_CREATE(&var_op, "");
		}
		free(name);
	}
}

void scr_return( struct code *pc, int fargc, char **fargv )
{
	if (pc->argc >= 1)
	{
		char *arg = eval_arg(pc->argv[0], fargc, fargv);
		VAR_STR_FORCE(local_vars, "?", arg);
		if (arg) free(arg);
	} else
	{
		VAR_STR_FORCE(local_vars, "?", "");
	}

	script_terminate=1;
}

void scr_jump( struct code *pc, int fargc, char **fargv )
{
	/* All the jumps and iff statements. */
	struct label *label;
	char *where;
	int new_argc;
	char **args;
	var_list_t locals;
	var_list_t *saved_locals;
	int i;
	char *result_var;

	/* First work out if we want to jump or not */
	if (!strcasecmp(pc->inst->name, "ifclear"))
	{
		compare_match=0;
		compare_count=0;
		return;
	}

	if (pc->argc<1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	if (script_debug) printf("- Conditional jump: %d compares, %d matches.\n", compare_count, compare_match);

	if (!strcasecmp(pc->inst->name, "ifall"))
	{
		if (compare_match < compare_count) return;
	}else
	if (!strcasecmp(pc->inst->name, "ifany"))
	{
		if (compare_match==0) return;
	}else
	if (!strcasecmp(pc->inst->name, "iffail"))
	{
		if (compare_match == compare_count) return;
	}else
	if (!strcasecmp(pc->inst->name, "ifnone"))
	{
		if (compare_match > 0) return;
	}
	where=eval_arg(pc->argv[0], fargc, fargv);

	/* check to see if there was a calling function to jump from, or if it
         * was a stand-alone instruction call
         */
	if (pc->parent == NULL)
	{
		if (script_debug) escprintf("- JUMP: Error in %s at %s jump point '%s' invalid.\n", pc->inst->name, pc->debug, where);
		return;
	}

	/* if yes, look through the functions label set, */
	/* if it matches, do a goto */
	label=pc->parent->jump;
	while (label!=NULL && strcasecmp(where, label->name)) label=label->next;

	if (label!=NULL)
	{
		script_jump=label->where;
		free(where);
		return;
	}

	/* otherwise, call ExecScript it will search for the function for us */
	VAR_NEWLIST(&locals);
	args = argv_shift(pc->argc, pc->argv, 0, &new_argc);
	if (strcasecmp(pc->inst->name, "call"))
	{
		result_var = 0;
		ARG_RANGE(&locals, 0, new_argc-1);
		for (i=0; i<new_argc; ++i)
			ARG_STR_FORCE(&locals, i, args[i]);
	} else {
		result_var = args[1];
		ARG_RANGE(&locals, 0, new_argc-2);
		ARG_STR_FORCE(&locals, 0, args[0]);
		for (i=2; i<new_argc; ++i)
			ARG_STR_FORCE(&locals, i-1, args[i]);
	}
	free(args); /* contained strings are now in the variable list */
	VAR_STR_FORCE(&locals, "?", "");
	saved_locals = local_vars;
	if (ExecScript(where, &locals, 0))
	{
		if (script_debug) escprintf("- JUMP: Error in %s at %s jump point '%s' invalid.\n", pc->inst->name, pc->debug, where);
		scr_devel_msg(pc, "Invalid function/label '%s'", where);
	}
	if (result_var)
	{
		var_op_t op;

		local_vars = saved_locals;
		VAR_OP_INIT(&op, &locals);
		VAR_SEARCH(&op, "?");
		if (VAR_FOUND(&op))
			var_str_force_2(result_var, var_str_val(&op));
		else
			var_str_force_2(result_var, "");
		free(result_var);
	}
	VAR_FREELIST(&locals);
	free(where);
}

void scr_end( struct code *pc, int fargc, char **fargv )
{
	script_terminate=2;
}

void scr_ison( struct code *pc, int fargc, char **fargv )
{
	char *who;
	int found = 0;

	who=eval_arg(pc->argv[0], fargc, fargv);
	if (who_find(who) != -1) found=1;

	compare_count++;
	if (found) {
		if (script_debug) printf("- ISON: found user '%s'\n", who);
		compare_match++;
	} else {
		if (script_debug) escprintf("- ISON: couldn't see '%s'\n", who);
	}

	free(who);
}

void scr_boolopt( struct code *pc, int fargc, char **fargv )
{
	char *what;
	int value=0;

	if (pc->argc<1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what=eval_arg(pc->argv[0], fargc, fargv);

	/* check what 'what' is, and set 'value' here */
	if(strlen(what)==1)
	{
		if(*what=='Y' || *what=='y')
			value = 1;
		if(*what=='n' || *what=='N')
			value = 0;
	}else if (!strcasecmp(what,"on")) {
		value = 1;
	}else if (!strcasecmp(what,"yes")) {
		value = 1;
	}else if (!strcasecmp(what,"true")) {
		value = 1;
	}else if (!strcasecmp(what,"off")) {
		value = 0;
	}else if (!strcasecmp(what,"no")) {
		value = 0;
	}else if (!strcasecmp(what,"false")) {
		value = 0;
	}else if (atoi(what)==1) {
		value = 1;
	}else if (atoi(what)==0) {
		value = 0;
	}else
		value = -1;

	compare_count++;

	if (value != -1)
	{
		if (script_debug) escprintf("- BOOLOPT: '%s' evaluated to '%d'\n", what, value);
		if (value) compare_match++;
	}
	else
		if (script_debug) escprintf("- BOOLOPT: Error invalid input '%s'\n", what);

	free(what);
}

void scr_isanum( struct code *pc, int fargc, char **fargv )
{
	char *what;
	int a;

	if (pc->argc<1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);

	compare_count++;

	if (!strcasecmp(pc->inst->name, "isanum"))
	{
		if (isanum(what, &a, 0)) compare_match++;
	} else if (!strcasecmp(pc->inst->name, "isadec"))
	{
		if (isanum(what, &a, 1)) compare_match++;
	}

	free(what);
}

void scr_inlist( struct code *pc, int fargc, char **fargv )
{
	char	*what;
	char	*cmp;
	int	i;
	int	icase = 0;

	if (pc->argc<2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);
	compare_count++;

	if (!strcasecmp(pc->inst->name, "iinlist")) icase = 1;

	for (i=1; i < pc->argc; i++)
	{
		cmp=eval_arg(pc->argv[i], fargc, fargv);
		if (icase)
		{
			if (!strcasecmp(what, cmp))
			{
				free(cmp);
				compare_match++;
				break;
			}
		}
		else
		{
			if (!strcmp(what, cmp))
			{
				free(cmp);
				compare_match++;
				break;
			}
		}
		free(cmp);
	}

	free(what);
}

void scr_range( struct code *pc, int fargc, char **fargv )
{
	char	*what, *aa, *bb;
	int	val, a, b;

	if (pc->argc<pc->inst->args) {
		if (script_debug) printf("Error in '%s' at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);
	aa=eval_arg(pc->argv[1], fargc, fargv);
	bb=eval_arg(pc->argv[2], fargc, fargv);
	compare_count++;

	if (isanum(aa, &a, 0) && isanum(bb, &b, 0) && isanum(what, &val, 0))
	{
		/* all okay */
	}
	else
	{
		if (script_debug) escprintf("- range: either '%s' (%s), '%s', or '%s' were not numbers\n", pc->argv[0], what, aa, bb);
		free(aa);
		free(bb);
		free(what);
		return;
	}

	/* check if aa, and bb are low/high respectively */
	if (a > b)
	{
		if (script_debug) escprintf("- range: arg2 (%s), is not less than or equal to arg3 (%s)\n", aa, bb);
		free(aa);
		free(bb);
		free(what);
		return;
	}

	/* number is in the range, so match */
	if ((val >= a) && (val <= b))
	{
		compare_match++;
	}

	free(what);
	free(aa);
	free(bb);
}

void scr_input( struct code *pc, int fargc, char **fargv )
{
	char *what;
	char *prompt;
	char *value;

	if (pc->argc < 1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);
	if (pc->argc >1)
		prompt=eval_arg(pc->argv[1], fargc, fargv);
	else
		prompt=strdup("? ");

	if (what==NULL || *what==0) {
		scr_devel_msg(pc, "target var not found");
		free(prompt);
		return;
	}

	busy++;
	if ((value=readline(prompt))==NULL) value=strdup("");
	busy--;

	var_str_force_2(what, value);
	free(value);
	free(what);
	if (prompt!=NULL) free(prompt);
}

void scr_regex( struct code *pc, int fargc, char **fargv )
{
#define MAX_REGMATCHES	100

	regmatch_t	pmatch[MAX_REGMATCHES];
	regex_t		preg;
	char		buff[80];
	int		replace=0;
	char		*pattern;
	char		*target;
	char		*a=NULL;
	char		*old_str;
	int		err;
	var_op_t	var;
	int		i;
	int		regex_flags = 0;

	if (!strcasecmp(pc->inst->name, "regex"))
	{
		if (pc->argc<2) {
			if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
			return;
		}
	}else if (!strcasecmp(pc->inst->name, "iregex"))
	{
		regex_flags |= REG_ICASE;
		if (pc->argc<2) {
			if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
			return;
		}
	}else if (!strcasecmp(pc->inst->name, "ised"))
	{
		regex_flags |= REG_ICASE;
		if (pc->argc<3) {
			if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
			return;
		}
		replace=1;
	}else if (!strcasecmp(pc->inst->name, "isedall"))
	{
		regex_flags |= REG_ICASE;
		if (pc->argc<3) {
			if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
			return;
		}
		replace=2;
	}else if (!strcasecmp(pc->inst->name, "sedall"))
	{
		if (pc->argc<3) {
			if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
			return;
		}
		replace=2;
	}else {
		if (pc->argc<3) {
			if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
			return;
		}
		replace=1;
	}

	target=eval_arg(pc->argv[0], fargc, fargv);
	pattern=eval_arg(pc->argv[1], fargc, fargv);
	if (replace) a=eval_arg(pc->argv[2], fargc, fargv);

	if ((err=regcomp(&preg, pattern, regex_flags)))
	{
		regerror(err, &preg, buff, 80);
		scr_devel_msg(pc, "%s", buff);
		free (target);
		free(pattern);
		if (replace) free(a);
		return;
	}

	if (replace)
	{
		/* locate the target variable */
		VAR_SEARCH_2(&var, local_vars, &var_list, target);

		if (!VAR_FOUND(&var))
		{
			if (script_debug) escprintf("- SED: cant find var '%s' for replace.\n", target);
			free (target);
			free(pattern);
			if (replace) free(a);
			return;
		}
		old_str = var_str_val(&var);
		if (script_debug) escprintf("- SED replace '%s' in $%s (%s) with '%s'.\n", pattern, target, old_str, a);

		if (replace == 1)
		{
			/* find the matches */
			if (regexec(&preg, var_str_val(&var), 1, pmatch, 0) != REG_NOMATCH)
			{
				/* make the substitution */
				if (pmatch[0].rm_so!=-1)
				{
					int newsize;
					char *stringy;
					newsize= strlen(old_str) - (pmatch[0].rm_eo - pmatch[0].rm_so) + strlen(a);
					stringy=(char *)malloc(newsize+1);
					strncpy(stringy, old_str, pmatch[0].rm_so);
					stringy[pmatch[0].rm_so]=0;
					strcat(stringy, a);
					strcat(stringy, &(old_str[pmatch[0].rm_eo]));
					VAR_STR_UPDATE(&var, stringy);
					free(stringy);
				}
			}
			else
			{
				if (script_debug) printf("- SED: No matches found\n");
			}
		}
		else if (replace == 2)
		{
			char	*oldstring;
			char	*teststr;
			int	j;
			int	unused;

			/* make backups of the string */
			oldstring = strdup(old_str);
			teststr = strdup(old_str);

			/* initialise the match structures */
			for (i = 0; i < MAX_REGMATCHES; i++)
			{
				pmatch[i].rm_so = -1;
				pmatch[i].rm_eo = -1;
			}

			/* find the first unused char in the pattern */
			for (unused = 1; unused < 256; unused++)
			{
				if (strchr(pattern, unused) == NULL) break;
			}

			/* no unused char allowed, so limit to one match */
			if (unused == 256) unused = 0;

			/* perform all matches */
			i = 0;
			while ((regexec(&preg, teststr, 1, pmatch, 0) != REG_NOMATCH) && (i < MAX_REGMATCHES))
			{
				if (i < MAX_REGMATCHES - 1)
				{
					pmatch[MAX_REGMATCHES - 1 - i].rm_so = pmatch[0].rm_so;
					pmatch[MAX_REGMATCHES - 1 - i].rm_eo = pmatch[0].rm_eo;
					pmatch[0].rm_so = -1;
					pmatch[0].rm_eo = -1;
				}
				for (j = pmatch[MAX_REGMATCHES - 1 - i].rm_so; j < pmatch[MAX_REGMATCHES - 1 - i].rm_eo; j++)
				{
					teststr[j] = unused;
				}
				i++;
			}

			/* give maximum replacement string error */
			if (i == MAX_REGMATCHES)
			{
				scr_devel_msg(pc, "Maximum regex matches found (%d). There may be other not replaced\n", i);
			}

			/* free the test string */
			free(teststr);

			/* make the changes */
			for (i = 0; i < MAX_REGMATCHES; i++)
			{
				if (script_debug) escprintf("- SED: string [%s], index [%d], pos: [%d/%d]\n", oldstring, i, pmatch[i].rm_so, pmatch[i].rm_eo);
				if (pmatch[i].rm_so != -1)
				{
					int	newsize;
					char	*newstring;

					newsize = strlen(oldstring) - (pmatch[i].rm_eo - pmatch[i].rm_so) + strlen(a);
					newstring = (char *)malloc(newsize+1);
					strncpy(newstring, oldstring, pmatch[i].rm_so);
					newstring[pmatch[i].rm_so] = 0;
					strcat(newstring, a);
					strcat(newstring, &(oldstring[pmatch[i].rm_eo]));

					free(oldstring);
					oldstring = newstring;
				}
			}

			/* update the new string */
			VAR_STR_UPDATE(&var, oldstring);
			free(oldstring);
		}
	}else
	{
		compare_count++;
		if (script_debug) escprintf("- REGEX: String '%s' pattern '%s'\n", target, pattern);
		if (regexec(&preg, target, 5, pmatch, 0 )==REG_NOMATCH) {
			if (script_debug) printf("- REGEX: match failed\n");
		}else {
			if (script_debug) printf("- REGEX: match succeeded\n");
			compare_match++;
		}
	}
	free(pattern);
	free(target);
	if (replace) free(a);
	regfree(&preg);

#undef MAX_REGMATCHES
}

void scr_string( struct code *pc, int fargc, char **fargv )
{
	char *what=NULL;
	char *part=NULL;
	char *text=NULL;
	char *value=NULL;
	char buff[81];

	if (!strcasecmp(pc->inst->name, "strlen") && pc->argc < 2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}else
	if (!strcasecmp(pc->inst->name, "strchr") && pc->argc < 3) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}else
	if (!strcasecmp(pc->inst->name, "strnchr") && pc->argc < 3) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}else
	if (!strcasecmp(pc->inst->name, "strstr") && pc->argc < 3) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}else
	if (!strcasecmp(pc->inst->name, "stristr") && pc->argc < 3) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}else
	if (!strcasecmp(pc->inst->name, "strmid") && pc->argc < 3) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);
	if (!strcasecmp(pc->inst->name, "strchr")
	|| !strcasecmp(pc->inst->name, "strnchr")
	|| !strcasecmp(pc->inst->name, "strmid")
	|| !strcasecmp(pc->inst->name, "strstr")
	|| !strcasecmp(pc->inst->name, "stristr")
	) {
		part=eval_arg(pc->argv[1], fargc, fargv);
		text=eval_arg(pc->argv[2], fargc, fargv);
	}else
		text=eval_arg(pc->argv[1], fargc, fargv);

	if (what==NULL || *what==0) {
		scr_devel_msg(pc, "target var not found");
		if (part!=NULL) free(part);
		if (text!=NULL) free(text);
		return;
	}

	if (!strcasecmp(pc->inst->name, "strlen"))
	{
		snprintf(buff, 80, "%zu", strlen(text));
		value=strdup(buff);
		if (script_debug) escprintf("- String length of '%s' is %s\n", text, value);
	}else
	if (!strcasecmp(pc->inst->name, "strchr"))
	{
		char  *d;
		int i;
		if ((d=strpbrk(text, part))==NULL) {
			if (script_debug) escprintf("- strchr: none of chars '%s' found in '%s'\n", part, text);
			value=strdup("-1");
		}else {
			i=d-text;
			snprintf(buff, 80, "%d", i);
			value=strdup(buff);
			if (script_debug) escprintf("- strchr: '%s' found at index %d in '%s'\n", part, i, text);
		}
	}else
	if (!strcasecmp(pc->inst->name, "strnchr"))
	{
		char  *d;
		int i;

		i=strspn(text, part);
		d = text+i;
		if (script_debug) escprintf("- strnchr: first char in '%s' not in '%s' is '%c' at location '%d'\n", text, part, *d, i);
		snprintf(buff, 80, "%d", i);
		value=strdup(buff);
	}else
	if (!strcasecmp(pc->inst->name, "strstr"))
	{
		char  *d;
		int i;

		if ((d=strstr(text, part))==NULL) {
			if (script_debug) escprintf("- strstr: substring '%s' not found in '%s'\n", part, text);
			value=strdup("-1");
		}else {
			i=d-text;
			snprintf(buff, 80, "%d", i);
			value=strdup(buff);
			if (script_debug) escprintf("- strstr: '%s' found at index %d in '%s'\n", part, i, text);
		}
	}else
	if (!strcasecmp(pc->inst->name, "stristr"))
	{
		char	*szText, *szNeedle;
		char	*d;
		int	i;

		/* allocate and lowercase the temporary search strings */
		szText = strdup(text);
		strlower(szText);
		szNeedle = strdup(part);
		strlower(szNeedle);

		if ((d=strstr(szText, szNeedle))==NULL)
		{
			if (script_debug) escprintf("- stristr: substring '%s' not found in '%s'\n", part, text);
			value=strdup("-1");
		}
		else
		{
			i=d - szText;
			snprintf(buff, 80, "%d", i);
			value=strdup(buff);
			if (script_debug) escprintf("- stristr: '%s' found at index %d in '%s'\n", part, i, text);
		}

		/* free the lowercase temporary strings */
		free(szText);
		free(szNeedle);
	}else
	if (!strcasecmp(pc->inst->name, "strmid"))
	{
		int start, end, i, size;
		char *c,  *frag;

		frag=part;
		size=strlen(text)-1;
		start=0;
		end=size;
		if ((c=strsep(&frag, "-:"))!=NULL) {
			i=atoi(c);
			if (strlen(c)==0) start=0; else start=i;

			/*
			if (strlen(c)==0 || i<0) start=0; else
			if (i>strlen(text)) start=size; else
			start=i;
			*/

			if (frag!=NULL) {
				i=atoi(frag);
				if (strlen(frag)==0) end=size; else end=i;

				/*
				if (strlen(frag)==0) end=size; else
				if (i<0) end=0; else
				if (i>size) end=size; else
				end=i;
				*/
			}
		}
		if (end < start)
		{
			value=strdup("");
			if (script_debug) printf("- strmid: end (%d) before start (%d), returning blank\n", end, start);
		}
		else
		{
			/* compact start/end sizes */
			if (start>strlen(text)) start=size; else if (start<0) start=0;
			if (end>size) end=size; else if (end<0) end=0;

			if (script_debug) escprintf("- strmid: copy to %s chars %d to %d of '%s'\n", what, start, end+1, text);
			text[end+1]=0;
			value=strdup( &text[start] );
		}
	}

	var_str_force_2(what, value);

	if (value!=NULL) free(value);
	if (what!=NULL) free(what);
	if (part!=NULL) free(part);
	if (text!=NULL) free(text);
}

void scr_case( struct code *pc, int fargc, char **fargv )
{
	char *what;
	char *value=NULL;
	int i, len;

	if (pc->argc < 2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what=eval_arg(pc->argv[0], fargc, fargv);
	value=eval_arg(pc->argv[1], fargc, fargv);
	len = strlen(value);

	if (!strcasecmp(pc->inst->name, "toupper"))
	{
		for (i=0; i < len; i++)
		{
			value[i] = toupper(value[i]);
		}
	}
	else if (!strcasecmp(pc->inst->name, "tolower"))
	{
		for (i=0; i < len; i++)
		{
			value[i] = tolower(value[i]);
		}
	}

	var_str_force_2(what, value);
	free(value);
	free(what);
}

void scr_base( struct code *pc, int fargc, char **fargv )
{
	char *what;
	char *value=NULL;

	if (pc->argc < 2) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	value=eval_arg(pc->argv[1], fargc, fargv);
	what=eval_arg(pc->argv[0], fargc, fargv);

	if (!strcasecmp(pc->inst->name, "toascii")) {
		int i;

		if (!isanum(value, &i, 0)) {
			if (script_debug) escprintf("- toascii: cant convert number '%s' to a char, cos its not a number\n", value);
			free(what);
			free(value);
			return;
		}
		if (script_debug) printf("- toascii: converting num %d to char '%c'\n", i, i);
		free(value);
		value=(char *)malloc(2);
		value[1]=0;
		value[0]=i;
	}else {
		char buff[20];
		snprintf(buff, 19, "%d", value[0]);
		if (script_debug) escprintf("- tovalue: converting char '%c' to num %s\n", value[0], buff);
		free(value);
		value=strdup(buff);
	}

	var_str_force_2(what, value);
	free(value);
	free(what);
}

void scr_user( struct code *pc, int fargc, char **fargv )
{
	char *what;
	char *value=NULL;

	if (pc->argc < 1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what=eval_arg(pc->argv[0], fargc, fargv);

	if (!strcasecmp(pc->inst->name, "user")) {
		if (event_user==NULL) {
			if (script_debug) printf(" - USER: Is not an event\n");
			value=strdup("");
		} else {
			if (script_debug) printf(" - USER: Event caused by %s\n", event_user);
			value=strdup(event_user);
		}
	}else if (!strcasecmp(pc->inst->name, "body"))
	{
		if (event_user==NULL) {
			if (script_debug) printf(" - BODY: Is not an event\n");
			value=strdup("");
		} else {
			if (script_debug) printf(" - BODY: Stripping to %zu chars of message text\n", strlen(event_body_text));
			value=strdup(event_body_text);
		}
	}else
	{
		value=strdup(user->record.name);
		if (script_debug) printf(" - WHOAMI: Your username is '%s'\n", value);
	}

	var_str_force_2(what, value);
	free(value);
	free(what);
}

void scr_version( struct code *pc, int fargc, char **fargv )
{
	char *what;

	if (pc->argc < 1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what=eval_arg(pc->argv[0], fargc, fargv);
	if (script_debug) printf(" - VERSION: Version string is '%s'", VERSION);
	var_str_force_2(what, VERSION);
	free(what);
}

void scr_room( struct code *pc, int fargc, char **fargv )
{
	char *what;
	int isquiet = !strcasecmp(pc->inst->name, "q-room") || u_god(user);

	if (pc->argc<1) return;
	if (!cm_test(user, CM_ONCHAT)) return;

	what=eval_arg(pc->argv[0], fargc, fargv);
	compare_count++;

	switch(ChangeRoom(what, isquiet))
	{
	case 0:
		/* success */
		compare_match++;
		break;
	case 1:
		if (!isquiet) display_message("You cannot find that room", 1, 1);
		break;
	case 2:
		if (script_debug) printf(" - ROOM: You are frozen, and cant change rooms\n");
		break;
	case 3:
		display_message("Sorry, you cannot go in that direction.\n", 1, 1);
		break;
	case 4:
		display_message("Invalid Room ID (0-65535 only)", 1, 1);
		break;
	}

	free(what);
}

void scr_sayto( struct code *pc, int fargc, char **fargv )
{
	int i, size, count;
	char *tmp, *p, *target;

	if (pc->argc < 2) return;
	if (!cm_test(user, CM_ONCHAT)) return;

	size=0;
	for (i=1; i<pc->argc; i++) size+=strlen(pc->argv[i]) + 1;

	/* overly generous allocation as later code isnt clean. */
	size+=512;
	tmp=malloc(sizeof(char) * (size+1));
	*tmp=0;

	count=0;
	for (i=1; i<pc->argc; i++)
	{
		if (i>1) {strcat(tmp, " "); count++; }
		p=eval_arg(pc->argv[i], fargc, fargv);
		count+=strlen(p);
		if (count>=size) { size=count+512; tmp=realloc(tmp, size); }
		strcat(tmp, p);
		free(p);
	}

	flood++;
	if (flood > flood_limit) {
		printf("FLOOD: This script has flooded the room. Terminating.\n");
		script_terminate=2;
		free(tmp);
		return;
	}

	target = eval_arg(pc->argv[0], fargc, fargv);
	const char *type = "says";
	if (tmp[strlen(tmp)-1] == '?') type = "asks";
	talk_sayto(tmp, target, type);
	free(target);
	free(tmp);
}

void scr_sendipc( struct code *pc, int fargc, char **fargv )
{
	int i, size, count;
	char *tmp, *p, *text;
	int bcast = 0;
	int startarg;

	if (!strcasecmp(pc->inst->name, "sendipb")) bcast = 1;

	if (bcast) {
		if (pc->argc < 1) return;
		startarg = 0;
	} else {
		if (pc->argc < 2) return;
		startarg = 1;
	}

	size=0;
	for (i=startarg; i<pc->argc; i++) size+=strlen(pc->argv[i]) + 1;

	/* overly generous allocation as later code isnt clean. */
	size+=512;
	tmp=malloc(sizeof(char) * (size+1));
	*tmp=0;

	count=0;
	for (i=startarg; i<pc->argc; i++)
	{
		if (i>startarg) {strcat(tmp, " "); count++; }
		p=eval_arg(pc->argv[i], fargc, fargv);
		count+=strlen(p);
		if (count>=size) { size=count+512; tmp=realloc(tmp, size); }
		strcat(tmp, p);
		free(p);
	}

	text = eval_arg(pc->argv[0], fargc, fargv);

	sendipc(text,tmp,bcast);

	free(text);
	free(tmp);
}

void scr_sendrpc( struct code *pc, int fargc, char **fargv )
{
	int i, size, count;
	char *text, *p, *sendto, *msg;
	int bcast = 0;
	int startarg;

	if (!strcasecmp(pc->inst->name, "sendrpb")) bcast = 1;

	if (bcast) {
		if (pc->argc < 2) return;
		startarg = 1;
	} else {
		if (pc->argc < 3) return;
		startarg = 2;
	}

	size=0;
	for (i=startarg; i<pc->argc; i++) size+=strlen(pc->argv[i]) + 1;

	/* overly generous allocation as later code isnt clean. */
	size+=512;
	text=malloc(sizeof(char) * (size+1));
	*text=0;

	count=0;
	for (i=startarg; i<pc->argc; i++)
	{
		if (i>startarg) {strcat(text, " "); count++; }
		p=eval_arg(pc->argv[i], fargc, fargv);
		count+=strlen(p);
		if (count>=size) { size=count+512; text=realloc(text, size); }
		strcat(text, p);
		free(p);
	}

	if (bcast)
	{
		sendto = strdup("");
		msg = eval_arg(pc->argv[0], fargc, fargv);
	}
	else
	{
		sendto = eval_arg(pc->argv[0], fargc, fargv);
		msg = eval_arg(pc->argv[1], fargc, fargv);
	}

	sendrpc(sendto,msg,text,bcast);

	free(text);
	free(msg);
	free(sendto);
}

void scr_listvars( struct code *pc, int fargc, char **fargv )
{
	static const char prefix_code[63] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	var_op_t op;
	int count=0, size=0, i;
	char *text, *nextkey;
	char *srch, *what;
	char *key;
	char lastkey[63];
	int prefix=0, searchlen=0;

	if (pc->argc < 1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}

	what=eval_arg(pc->argv[0], fargc, fargv);

	if (pc->argc >= 2)
	{
		srch = eval_arg(pc->argv[1], fargc, fargv);
		searchlen = strlen(srch);
	}
	else
		srch = 0;

	i = 0;
	size+=512;
	text = malloc(sizeof(char) * (size+1));
	lastkey[0] = '\0';
	text[0] = '\0';
	VAR_LIST_ITERATE(&op, &var_list);
	while (VAR_FOUND(&op))
	{
		key = VAR_STR_KEY(&op);
		if (srch == NULL || !strncasecmp(srch, key, searchlen))
		{
			/* How many initial chars match the previous key? */
			prefix = 0;
			while (lastkey[prefix] && lastkey[prefix] == key[prefix])
				++prefix;

			count+=strlen(key+prefix);
			if (count>=size) { size=count+512; text=realloc(text, size); }
			if (i>0) {strcat(text, " "); count++; }
			nextkey = text + strlen(text);
			snprintf(nextkey, text+size-nextkey-1, "%c%s", prefix_code[prefix], key+prefix);
			snprintf(lastkey, 62, "%s", key);
			++i;
		}

		VAR_LIST_NEXT(&op);
	}
	var_str_force_2(what, text);
	if (srch) free(srch);
	free(text);
	free(what);
}


void scr_logoninfo( struct code *pc, int fargc, char **fargv )
{
	char		*what;
	char		value[20];
	extern int	talker_logontype;
	int		pos;

	if (pc->argc < 1) {
		if (script_debug) printf("Error in %s at %s too few arguments.\n", pc->inst->name, pc->debug);
		return;
	}
	what=eval_arg(pc->argv[0], fargc, fargv);

	if (script_debug) printf(" - LOGONINFO: Login mask: 0x%08x\n", talker_logontype);
	pos = 0;
	if (talker_logontype & LOGONMASK_QUIET) value[pos++] = 'Q';
	if (talker_logontype & LOGONMASK_SUMMONED) value[pos++] = 'S';
	value[pos] = 0;

	var_str_force_2(what, value);
	free(what);
}

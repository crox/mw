#ifndef LOG_H
#define LOG_H

void catchuri(const char *what);
void catchdoing(const char *what);

struct block_t {
	char *p_buffer; 	/* the actual data block */
	int i_size;		/* actual block size */
	int i_limit;		/* max size, 0=unlimited */

	int     i_used;		/* current fill level */
	char *p_cursor;		/* user pointer */
};

struct block_t * block_new(int);
void block_free(struct block_t *);
void block_resize(struct block_t *, int);
void block_limit(struct block_t *, int);
int block_append(struct block_t *p, void *data, int size);

#endif /* LOG_H */

#ifndef CLIENT_TALKER_PRIVS_H
#define CLIENT_TALKER_PRIVS_H

#include <talker_privs.h>

char *part_gag_filter(const char *text, int state);
unsigned long chatmode_describe(unsigned long old, unsigned long new, int ourapl, int theirapl, const char *from);

#endif /* CLIENT_TALKER_PRIVS_H */

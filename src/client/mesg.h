#ifndef MESG_H
#define MESG_H

#include <mesg.h>

void broadcast(int state, const char *fmt, ...) __attribute__((format(printf,2,3)));
void inform_of_mail(char *to);
void postinfo(struct user *who, struct folder *fol, struct Header *mesg);
void mwlog(const char *fmt, ...) __attribute__((format(printf,1,2)));

#endif /* MESG_H */

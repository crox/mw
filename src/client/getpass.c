/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <stdio.h>
#include "echo.h"
#include "getpass.h"

extern int internet;

char *get_pass(char *p)
{
	static char buf[9];
	int pos=0;
	printf("%s",p);
	fflush(stdout);
	echo_off();
	while(1)
	{
		int c;

		while ((c=getchar())==255) {
			getchar();
			getchar();
		}
		if(c==EOF) {
			buf[0] = '\0';
			return(buf);
		}
		if(pos&&(c==8||c==127))
			pos--;
		else if(c==10 || c==13)
		{
			buf[pos]=0;
			echo_on();
			if(internet==0)
			{
				printf("\r\n");
				fflush(stdout);
			}else
			{
				if(c==13)
					getchar();
				putchar('\n');
			}
			return(buf);
		}
		else if(pos<8&&c!='\r')
			buf[pos++]=c;
	}
}

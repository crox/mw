/*
 * Init file handling
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
#include <curl/curl.h>

#include <sqlite.h>
#include "sqlite.h"
#include "intl.h"
#include "alias.h"
#include "js.h"
#include "init.h"
#include "user.h"
#include "script.h"
#include "frl.h"

extern struct user * const user;

/* drop and restore user level privs */
static int private_myid = -1;

int perms_drop(void)
{
	private_myid=geteuid();
	if (seteuid(getuid()) == -1) return -1;
	return private_myid;
}

void perms_restore(void)
{
	if (private_myid != -1) {
		seteuid(private_myid);
		private_myid = -1;
	}
}

static int ReadInitFile(const char *base, const char *filename)
{
	FILE *file;
	char *buff, *backup, *header;
	char path[1024];
	char *a, *b, *c;
	int lineno;
	struct stat stats;

	if (strncasecmp(filename, "http://", 7)==0
	||  strncasecmp(filename, "https://", 8)==0) {
		CURL *cl;
		char cerr[CURL_ERROR_SIZE];
		/* use libcurl to grab the file */
		perms_drop();
		file = tmpfile();
		if (file == NULL) {
			fprintf(stderr, "Error opening temporary file\n");
		}
		cl = curl_easy_init();
/*		curl_easy_setopt(cl, CURLOPT_NOPROGRESS, 0); */
		curl_easy_setopt(cl, CURLOPT_WRITEDATA, file);
		curl_easy_setopt(cl, CURLOPT_URL, filename);
		curl_easy_setopt(cl, CURLOPT_ERRORBUFFER, cerr);
		curl_easy_setopt(cl, CURLOPT_USERAGENT, "Milliways III v" VERSION);
		if (curl_easy_perform(cl))
			fprintf(stderr, "Error loading %s: %s\n", filename, cerr);
		curl_easy_cleanup(cl);
		perms_restore();
		fseek(file, 0, SEEK_SET);
	} else {
		if (filename[0] == '/' ||
		    !strncmp(filename, "../", 3) ||
		    strstr(filename, "/../"))
		{
			printf(_("Cannot load \"%s\": Illegal path\n"), filename);
			return 1;
		}
		snprintf(path, 1023, "%s/%s", base, filename);
		perms_drop();
		if (stat(path, &stats))
		{
			/* be quiet about its not there, handle higher up */
			perms_restore();
			return 1;
		}
		if (!S_ISREG(stats.st_mode))
		{
			printf(_("Error reading %s: Not a regular file\n"), path);
			perms_restore();
			return 1;
		}

		if ((file=fopen(path,"r"))==NULL)
		{
			if (strcmp(".mwrc", filename))	printf(_("Error reading %s: %s\n"), path, strerror(errno));
			perms_restore();
			return 1;
		}
		perms_restore();
	}

	if ((a=strrchr(filename, '.'))!=NULL && strncasecmp(a, ".js", 3)==0) {
		load_jsfile(file, filename);
		fclose(file);
		return 0; // changed because if an error occured after this point the file exists, the js code has reported the error to the user and returning 1 will report to them that it doesn't exist
	}

	lineno=0;
	while (!feof(file))
	{
		if ((buff = frl_line_nspace(file, "#")) == NULL) break;
		backup = buff;
		lineno+=num_lines_read();
		if (strchr("\n\r#", buff[0])!=NULL) { free(backup); continue; }

		header = strdup(backup);
		if ((a=strtok(buff," \t"))==NULL) { free(backup); free(header); continue; }

		if (*a=='#') { free(backup); free(header); continue; }

		/* load command aliases */
		if (!strcasecmp(a,"alias"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed alias in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			if ((c=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed alias in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}

			if (AddLink(&alias_list, b, c))
			{
				printf(_("Alias %s already exists. Redefined at line %d in %s.\n"), b, lineno, filename);
			}
		}else
		/* load function binds */
		if (!strcasecmp(a,"bind"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed bind in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			if ((c=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed bind in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}

			if (AddLink(&bind_list, b, c))
			{
				printf(_("Bind %s already exists. Redefined at line %d in %s.\n"), b, lineno, filename);
			}
		}else
		/* load rpc binds */
		if (!strcasecmp(a,"rpc"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed rpc bind in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			if ((c=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed rpc bind in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}

			if (AddLink(&rpc_list, b, c))
			{
				printf(_("RPC Bind %s already exists. Redefined at line %d in %s.\n"), b, lineno, filename);
			}
		}else
		/* include a seperate script file */
		if (!strcasecmp(a, "include"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed include in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			if ((c=strrchr(filename,'/'))!=NULL) {
				char rpath[1024];
				*c=0;
				snprintf(rpath,sizeof(rpath),"%s/%s", filename, b);
				*c='/';
				ReadInitFile(base, rpath);
			}else
				ReadInitFile(base, b);
		}else
		/* check for destroying functions */
		if (!strcasecmp(a, "destroy"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed include in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			if (!strcasecmp(b, "*")) DestroyAllFunctions(0); else DestroyFunction(b);
		}else
		/* bind event function */
		if (!strcasecmp(a, "event"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed event bind in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}

			if (AddLink(&event_list, b, ""))
			{
				printf(_("Event bind already exists. Useless instruction at line %d in %s.\n"), lineno, filename);
			}
		}else
		/* bind ipc function */
		if (!strcasecmp(a, "ipc"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed ipc in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}

			if (AddLink(&ipc_list, b, ""))
			{
				printf(_("IPC bind already exists. Useless instruction at line %d in %s.\n"), lineno, filename);
			}
		}else
		/* allow user to bind log on/off functions */
		if (!strcasecmp(a, "checkonoff"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed checkonoff bind in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}

			if (AddLink(&onoff_list, b, ""))
			{
				printf(_("Checkonoff bind already exists. Useless instruction at line %d in %s.\n"), lineno, filename);
			}
		}else
		/* allow user to bind shutdown functions */
		if (!strcasecmp(a, "shutdown"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed shutdown bind in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}

			if (AddLink(&shutdown_list, b, ""))
			{
				printf(_("Shutdown bind already exists. Useless instruction at line %d in %s.\n"), lineno, filename);
			}
		}else
		/* allow user to bind force functions */
		if (!strcasecmp(a, "force"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed force bind in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}

			if (AddLink(&force_list, b, ""))
			{
				printf(_("Force bind already exists. Useless instruction at line %d in %s.\n"), lineno, filename);
			}
		}else
		/* load mw-script functions */
		if (!strcasecmp(a,"function"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed Script Function declaration in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			/* set function to 'normal' */
			LoadFunction(b, header, file, &lineno, filename, 0);
		}else
		/* load mw-script functions (local, hidden function) */
		if (!strcasecmp(a,"lfunction"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed Script Function declaration in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			/* set function to 'non-interface' (local) */
			LoadFunction(b, header, file, &lineno, filename, FUNCFLAG_LOCAL);
		}else
		/* load mw-script initialisation function */
		if (!strcasecmp(a,"initfunc"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed Script Init declaration in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			/* set function to 'init' */
			LoadFunction(b, header, file, &lineno, filename, FUNCFLAG_AUTOINIT);
		}else
		/* load a local, hidden mw-script initialisation function */
		if (!strcasecmp(a,"linitfunc"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed Script Init declaration in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			/* set function to 'init' and 'non-interface'*/
			LoadFunction(b, header, file, &lineno, filename, FUNCFLAG_AUTOINIT | FUNCFLAG_LOCAL);
		}else
		/* load mw-script board initialisation function */
		if (!strcasecmp(a,"boardfunc"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed Script BoardInit declaration in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			/* set function to 'boardinit' */
			LoadFunction(b, header, file, &lineno, filename, FUNCFLAG_BOARDINIT);
		}else
		/* load a local, hidden mw-script board initialisation function */
		if (!strcasecmp(a,"lboardfunc"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed Script BoardInit declaration in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			/* set function to 'init' and 'non-interface'*/
			LoadFunction(b, header, file, &lineno, filename, FUNCFLAG_BOARDINIT | FUNCFLAG_LOCAL);
		}else
			printf(_("Loading file %s unrecognised command '%s' on line %d\n"),filename, a, lineno);
		free(backup);
		free(header);
	}
	fclose(file);
	return 0;
}

void LoadInitFile(const char *name)
{
	char *mwrc = NULL;
	struct passwd *pw;
	int dofree=0;

	if (name == NULL) {
		mwrc = userdb_get(USERDB_PRIVATE, user->record.name, "mwrc");
		if (mwrc != NULL && mwrc[0]!=0) {
			name = mwrc;
			dofree=1;
		}
		if (mwrc != NULL && mwrc[0]==0)
			free(mwrc);
	}
	if (name == NULL) name = ".mwrc";

	if ((pw=getpwuid(getuid())) == NULL) {
		fprintf(stderr, _("Failed to get user data\n"));
		goto out;
	}
	/* try to load the personal copy*/
	if (!ReadInitFile(pw->pw_dir, name)) {
		goto out;
	}

	/* try the system wide one instead */
	if (ReadInitFile(HOMEPATH"/scripts", name)) {
		if (strcmp(".mwrc", name)!=0)
			fprintf(stderr, _("Could not find init file %s\n"), name);
	}
out:
	if (dofree) free(mwrc);
}



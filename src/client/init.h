#ifndef INIT_H
#define INIT_H

int perms_drop(void);
void perms_restore(void);
void LoadInitFile(const char *name);

#endif /* INIT_H */

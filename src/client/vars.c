/* vars.c
 * Functions for manipulating variable lists.
 * Includes linked-list and hashtable versions.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "expand.h"

char *var_str_val(var_op_t *op)
{
    static char numtext[20];

    if (VAR_TYPE_STR(op))
        return ((char *)(**(op)->rec).data.pdata);
    else
    {
        snprintf(numtext, 19, "%d", VAR_INT_VAL(op));
	return numtext;
    }
}

void var_key_dup(var_op_t *op)
{
    op->key = strdup(op->key);
}

int arg_count(var_list_t *list)
{
    var_op_t op;

    VAR_OP_INIT(&op, list);
    VAR_SEARCH(&op, "#");
    if (!VAR_FOUND(&op))
        return 0;
    else if (!VAR_TYPE_INT(&op))
        return 0;
    else
        return VAR_INT_VAL(&op);
}

void var_str_force_2(const char *name, const char *val)
{
    var_op_t op;

    if (eval_var(name, &op))
    {
	if (VAR_FOUND(&op))
	{
	    if (script_debug) printf("- updating var $%s (%s) to '%s'\n", name, var_str_val(&op), val);
	    VAR_STR_UPDATE(&op, val);
	} else
	{
	    if (script_debug) printf("- creating var $%s setting to '%s'\n", name, val);
	    VAR_STR_CREATE(&op, val);
	}
	VAR_KEY_FREE(&op);
    }
}

int var_isanum(var_op_t *op, int *result, int onlydecimal)
{
    extern int isanum(char *a, int *result, int onlydecimal);

    if (VAR_FOUND(op))
    {
        if (VAR_TYPE_INT(op))
	{
	    *result = VAR_INT_VAL(op);
	    return 1;
	} else if (VAR_TYPE_STR(op))
	{
	    return isanum(var_str_val(op), result, onlydecimal);
	}
    }
    return 0;
}


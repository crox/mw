#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>
#include "alarm.h"
struct alarm *volatile first_alarm, *volatile next_alarm;
static const struct timeval zero_time = {0, 0};
static int alarm_enabled = 0;
static volatile int alarm_pending = 0;

static void alarm_reset(void)
{
	struct sigaction s;
	sigset_t mask;

	sigemptyset(&mask);
	s.sa_handler = &alarm_handler;
	s.sa_mask = mask;
	s.sa_flags = SA_RESTART;
	sigaction(SIGALRM, &s, NULL);
}

void alarm_enable(void)
{
	alarm_enabled = 1;
}

void alarm_disable(void)
{
	alarm_enabled = 0;
}

void alarm_init(void)
{
	first_alarm = next_alarm = NULL;
	alarm_disable();
	alarm_reset();
}

void alarm_cleanup(void)
{
	struct sigaction s;
	sigset_t mask;

	alarm_disable();
	alarm(0);
	sigemptyset(&mask);
	s.sa_handler = SIG_DFL;
	s.sa_flags = 0;
	s.sa_mask = mask;
	sigaction(SIGALRM, &s, NULL);
	next_alarm = NULL;
	while (first_alarm != NULL)
	{
		next_alarm = first_alarm->next;
		free(first_alarm);
		first_alarm = next_alarm;
	}
}

void alarm_handler(int sig)
{
	if (alarm_enabled)
	{
		alarm_check();
	} else
	{
		alarm_pending++;
		alarm_reset();
	}
}

void alarm_check(void)
{
	struct timeval now;
	struct itimerval delay;

	gettimeofday(&now, NULL);
	while (next_alarm != NULL && timercmp(&next_alarm->when, &now, <=))
	{
		if (next_alarm->how != NULL)
			(*next_alarm->how)(next_alarm->what);
		next_alarm = next_alarm->next;
		gettimeofday(&now, NULL);
	}
	if (next_alarm == NULL)
	{
		alarm_pending = 0;
		return;
	}
	timerclear(&delay.it_interval);
	timersub(&next_alarm->when, &now, &delay.it_value);
	alarm_reset();
	if (timercmp(&delay.it_value, &zero_time, <=))
		{if (alarm_enabled) raise(SIGALRM);}
	else
		setitimer(ITIMER_REAL, &delay, NULL);
}

struct alarm *alarm_after(long secs, long usecs, void *what, void (*how)(void *))
{
	struct timeval when, delay, now;

	gettimeofday(&now, NULL);
	delay.tv_sec = secs + usecs / 1000000;
	delay.tv_usec = usecs % 1000000;
	timeradd(&now, &delay, &when);
	return alarm_at(&when, what, how);
}

struct alarm *alarm_at(const struct timeval *when, void *what, void (*how)(void *))
{
	struct alarm *temp, *volatile *next, *prev;
	struct timeval now;
	struct itimerval it_delay;

	/* Delete events which have already been processed */
	while (next_alarm != first_alarm)
	{
		temp = first_alarm;
		first_alarm = first_alarm->next;
		free(temp);
	}

	/* Insert the new event in chronological order */
	prev = next_alarm;
	next = &next_alarm;
	while (*next != NULL && timercmp(&(**next).when, when, <)) next = &(**next).next;
	temp = (struct alarm *)malloc(sizeof *temp);
	temp->when = *when;
	temp->what = what;
	temp->how = how;
	temp->next = *next;
	*next = temp;
	first_alarm = next_alarm;

	if (prev == NULL) alarm_reset();
	if (temp != next_alarm) return temp;

	/* Set a new alarm for the first event, if this is the one we added */
	gettimeofday(&now, NULL);
	timersub(&next_alarm->when, &now, &it_delay.it_value);
	timerclear(&it_delay.it_interval);
	if (timercmp(&it_delay.it_value, &zero_time, <=))
		raise(SIGALRM);
	else
		setitimer(ITIMER_REAL, &it_delay, NULL);
	return temp;
}

static void set_flag(void *flag)
{
	if (flag != NULL) *(int *)flag = 1;
}

void alarm_sleep(struct timeval *delay, int resume)
{
	volatile int flag;
	struct alarm *alrm;
	struct timeval finish;

	flag = 0;
	do {
		alrm = alarm_after(delay->tv_sec, delay->tv_usec, (void *)&flag, &set_flag);
		select(0, NULL, NULL, NULL, delay);
		if (flag)
			timerclear(delay);
		else
		{
			gettimeofday(&finish, NULL);
			alrm->how = NULL;
			timersub(&alrm->when, &finish, delay);
			if (timercmp(delay, &zero_time, <=))
			{
				delay->tv_sec = 0;
				delay->tv_usec = 1;
			}
		}
	} while (!flag && resume);
}

int alarm_select(int maxfd, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout)
{
	int nfds;
	int select_error;
	struct timeval temp, now;

	gettimeofday(&now, NULL);
	if (next_alarm != NULL)
	{
		timersub(&next_alarm->when, &now, &temp);
		if (timercmp(timeout, &temp, > ))
			memcpy(timeout, &temp, sizeof(temp));
		if (timercmp(timeout, &zero_time, <))
			timerclear(timeout);
	}
	timeradd(&now, timeout, &temp);
	alarm_disable();
	alarm_check();
	nfds = select(maxfd, readfds, writefds, exceptfds, timeout);
	select_error = errno;
	alarm_check();
	errno = select_error;
	gettimeofday(&now, NULL);
	timersub(&temp, &now, timeout);
	if (nfds == 0)
		timerclear(timeout);
	else if (timercmp(timeout, &zero_time, <=))
	{
		timeout->tv_sec = 0;
		timeout->tv_usec = 1;
	}
	return nfds;
}

/* This one doesn't work...
int alarm_select(int maxfd, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout)
{
	struct timeval now, remaining, newtimeout;
	int nfds;
	if (next_alarm != NULL)
	{
		gettimeofday(&now, NULL);
		timersub(&next_alarm->when, &now, &remaining);
		if (timercmp(&remaining, &zero_time, <=)) {
			timerclear(&remaining);
		}
		if (timercmp(&remaining, timeout, <=)) {
			timersub(timeout, &remaining, &newtimeout);
			nfds = select(maxfd, readfds, writefds, exceptfds, &remaining);
			alarm_check(&now);
			memcpy(timeout, &newtimeout, sizeof(struct timeval));
			printf("%c", nfds+'0');
			return nfds;
		}
	}
	return select(maxfd, readfds, writefds, exceptfds, timeout);
}
*/

/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <stdio.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>
#include <time.h>
#include <stdbool.h>
#include "special.h"
#include "talker_privs.h"
#include "talker.h"
#include "str_util.h"
#include "getpass.h"
#include "perms.h"
#include "read.h"
#include "who.h"
#include "echo.h"
#include "util.h"
#include "intl.h"
#include "userio.h"
#include "user.h"
#include "mesg.h"

const char *partlist_search[]={
"status", "special", "groups", "chatprivs", "chatmode", "protpower",
"protlevel", NULL};

extern struct user * const user;
extern int internet;

char *getmylogin(void)
{
	struct passwd *pw;
	if ((pw=getpwuid(getuid()))==NULL)
		return NULL ;
	else
	if (strcmp(pw->pw_name, "bbs")==0)
		return NULL;
	else
		return(pw->pw_name);
}

static int old_usr(struct user *usr)
{
	char salt[3],passwd[PASSWDSIZE];
	char *name;

	name=(char *)getmylogin();
	if (name == NULL || !stringcmp(usr->record.name, name, -1))
	{
		strncpy(salt, usr->record.passwd, 2);
		fflush(stdin);
		strcpy(passwd,crypt(get_pass(_("Enter password: ")),salt));
		if (strcmp(passwd, usr->record.passwd))
		{
			printf(_("Login incorrect.\n\n"));
			exit(0);
		}
	}
	if (u_ban(usr))
	{
		printf(_("Sorry, this username has been banned.\n"));
		printf(_("Have a nice day. *:-)\n"));
		exit(0);
	}
	printf(_("Hello %s.\n"), usr->record.name);
	return 1;
}

void strip_name(char *string)
{
	char *array;
	int i,ptr=0;
	int len;

	len=strlen(string);
	array=(char *)malloc(len+1);
	for (i=0;i<len;i++)
	{
		if (isascii(string[i]) && (isalnum(string[i]) || string[i]=='_' || string[i]=='-'))
			array[ptr++]=string[i];
	}
	array[ptr]=0;
	strcpy(string,array);
	free(array);
}

void get_login(char *name, int autochat)
{
	char *lname = getmylogin();
	fflush(stdin);
gstart:
	if (lname == NULL) {
		/* cant find you, or you are bbs, MUST tell us name */
		printf(_("Please enter username: "));
		get_str(name,NAMESIZE);
	} else {
		/* we got a name from the system,
		 * prompt only if not autochat mode */
		if (autochat) {
			name[0]=0;
		} else {
			printf(_("Please enter username [%s]: "), lname);
			get_str(name,NAMESIZE);
		}
	}
	strip_name(name);
	if (feof(stdin))
		exit(0);

	if (strlen(name)<1)
	{
		if (lname==NULL)
		{
			printf(_("Don't be shy.\n"));
			goto gstart;
		}else
		{
			strncpy(name,lname,NAMESIZE);
			name[NAMESIZE]=0;
		}
	}
}

void pick_salt(char *salt)
{
	strcpy(salt,"ab");
}

static int set_defaults(struct person *tmp)
{
	int		i, n;
	struct folder	f;
	struct Header	head;
	long		curtime = time(NULL);
	int ret = 0;

	/* set default colour scheme. wont effect users if they dont have colour on, obviously */
	tmp->colour=0;
	/* clear all statuses */
	tmp->status=0;
	tmp->status |= (1<<6); /* beeps off */
	/* enable tab completion special only */
	tmp->special=(1<<12);
	/* not registered to any groups as standard */
	tmp->groups=0;
	/* first login time to now */
	tmp->lastlogout=time(0);
	/* bugger all usage :) */
	tmp->timeused=0l;
	/* no talker modes or privileges */
	tmp->chatmode=0;
	tmp->chatprivs=0l;
	/* default timeout value */
	tmp->timeout = 86400;
	tmp->doing[0]=0;
	tmp->dowhen=0;


/********************************************
IT IS HERE WE DO THE AUTO-CATCHUP-ALL STUFF
MAX OF 10 MESSAGES IN THE LAST 2 WEEKS
********************************************/

	/* all folder messages unread */
	tmp->folders[0]=SETALLLONG;
	tmp->folders[1]=SETALLLONG;
	for (i=0;i<64;i++) tmp->lastread[i]=0;

	/* only do stuff if we can open folder info file */
	if ((n = openfolderfile(O_RDONLY)) >= 0)
	{
		i=0;
		/* go through each folder */
		while (get_folder_entry(n, &f) > 0)
		{
			if (f_active(f.status))
			{
				int idx;
				/* get index of max of last 10 messages */
				if (f.last - 10 > f.first)
					idx = f.last - 10;
				else
					idx = f.first;
				/* now work forwards til have a message less than 2 weeks old */
				/* 2 weeks = 60 secs * 60 mins * 24 hours * 14 days */
				/*         = 1209600 secs */
				while (get_mesg_header(&f, idx, &head)
					&& (curtime - head.date > 1209600)) idx++;
				/* set last read message */
				user->record.lastread[i] = idx;
			}
			i++;
		}
		/* close info file */
		close(n);
	}

/***********/

	/* if run as owner of the program, allow 'backdoor' SU access	 */
	if (getuid()==geteuid()&&!internet) /* if you have run it as the owner */
	{
		char buff[11];
		printf(_("What status do you want to be ? [rms] "));
		get_str(buff,10);
		if (buff[0] == '\0')
		{ /* User just hit return so use the defaults */
			strcpy(buff, "rms");
		}
		tmp->status=user_stats(buff,0);
		show_user_stats(tmp->status,buff,false);
		printf(_("Status set to [%s]\n"),buff);
	/* if you have a SUCS account, allow auto-registering if the logon name matches the sucs name */
	}else if (!internet || getmylogin()==NULL || (getmylogin()!=NULL && strcasecmp(getmylogin(),tmp->name)==0))
	{
		tmp->status|=1;
		if (getmylogin() != NULL) {
			tmp->groups|=1;
			tmp->chatprivs|=CP_SCRIPT;
		}
		mwlog("AUTOREGISTER %s",getmylogin());
		ret = 1; /* Autoregistered */
	}
	return ret;
}

static int new_usr(char *name, struct user *u)
{
	char passwd[PASSWDSIZE],passwd2[PASSWDSIZE],salt[3];
	struct person *usr = &u->record;
	char c[10];
	do{
		printf(_("Did I get your name right %s ? [Y]/n: "),name);
		get_str(c,9);
		if (c[0] == '\0')
		{ /* User just hit return so use the default answer */
			strcpy(c, "y");
		}
	}while (!*c);
	if(*c=='y' || *c=='Y')
	{
		int diff=false;

		pick_salt(salt);
		printf(_("We use a password on this BB.\n"));
		fflush(stdout);
		do{
			if (diff) printf(_("Passwords did not match.\n"));
			strcpy(passwd,crypt(get_pass(_("Enter password: ")),salt));
			strcpy(passwd2,crypt(get_pass(_("Re-enter password: ")),salt));
		}while ((diff=strcmp(passwd,passwd2)));
		strncpy(usr->passwd,passwd,PASSWDSIZE);
		strncpy(usr->name,name,NAMESIZE);

		printf(_("\nPlease enter the following details so that we can register you as a\n"));
		printf(_("normal user of this bulletin board. without correct information you\n"));
		printf(_("will not be allowed to use the full facilities of this board.\n"));
		printf(_("\nDATA PROTECTION ACT:\n"));
		printf(_("Any data entered will be recorded in a computer database for the purpose\n"));
		printf(_("of the administration, operation and security of the computer society. By \n"));
		printf(_("entering this data you consent to the storage of this data, and become an\n"));
		printf(_("associate member of the society.\n"));
		printf(_("\nIf you do not wish to register, do not enter a name.\n\n"));


		printf(_("Real name: "));
		usr->realname[0]=0;
		get_str(usr->realname,REALNAMESIZE);
		if (!usr->realname[0])
		{
			printf(_("User record '%s' cancelled.\n"),usr->name);
			printf(_("Goodbye.\n"));
			exit(0);
		}
		strip_str(usr->realname);
		printf(_("Email address: "));
		usr->contact[0]=0;
		get_str(usr->contact,CONTACTSIZE);
		strip_str(usr->contact);

		mwlog("CREATED: %s <%s>",usr->realname, usr->contact);

		int ret = 2; /* New user */
		ret += set_defaults(usr);
		printf(_("Creating new user %s\n"),name);

		userdb_write(u);
		return ret;
	}
	else return 0;
}

/**
 * Returns: 1: Existing user
 *          2: New user
 *          3: New user, autoregistered
 */
int login_ok(struct user *usr, int *autochat)
{
	char name[NAMESIZE+1];
	int ret = 0;
	do{
		get_login(name, *autochat);
		if(user_exists(name, usr))
			ret = old_usr(usr);
		else
			ret = new_usr(name, usr);
	} while (!ret);
	return ret;
}

void list_users(int newonly)
{
	int err;
	int file;
	struct user usr;
	char stats[10],gr[10];
	int linecount=0;
	int screen_height = screen_h();
	long logout_time;

	file=userdb_open(O_RDONLY);
	if (file < 0)
		return;

	for_each_user(&usr, file, err) {
		if (!newonly || ( newonly && !u_reg(&usr) ) )
		{
			logout_time = usr.record.lastlogout;
			show_user_stats(usr.record.status, stats, true);
			show_fold_groups(usr.record.groups, gr, true);
			if (!u_del(&usr))
			{
				linecount++;
				printf("%*s [%5s] [%8s] %s", NAMESIZE, usr.record.name,
				     stats, gr, ctime(&logout_time));
				if (linecount>=(screen_height-1))
				{
					char foo[6];
					printf(_("---more---\r"));
					echo_off();
					get_str(foo,5);
					echo_on();
					if (foo[0]=='q'|| foo[0]=='Q') break;
					printf("            \r");
					linecount=0;
				}
			}
		}
	}
	close(file);
}

void list_users_since(long date)
{
	int err;
	int file;
	struct user usr;
	struct person *urec = &usr.record;
	char stats[10],gr[10];
	int linecount=0;
	int screen_height = screen_h();
	long logout_time;

	file=userdb_open(O_RDONLY);
	if (file < 0)
		return;

	for_each_user(&usr, file, err) {
		if (urec->lastlogout > date)
		{
			logout_time = urec->lastlogout;
			show_user_stats(urec->status, stats, true);
			show_fold_groups(urec->groups, gr, true);
			if (!u_del(&usr))
			{
				linecount++;
				printf("%*s [%5s] [%8s] %s", NAMESIZE, urec->name,
				      stats, gr, ctime(&logout_time));
				if (linecount>=(screen_height-1))
				{
					char foo[6];
					printf(_("---more---\r"));
					echo_off();
					get_str(foo,5);
					echo_on();
					if (foo[0]=='q'|| foo[0]=='Q') break;
					printf("            \r");
					linecount=0;
				}
			}
		}
	}
	close(file);
}

static void list_users_flags(int flags, int type, int inv)
/*type: 0=status 1=special 2=group 3=chatmodes 4=chatprivs */
/*inv: 0= with flags, 1=without flags */
{
	int err;
	int file;
	struct user usr;
	struct person *urec = &usr.record;
	char stats[10],gr[10],spec[20],cmodes[20],cprivs[20],prot[10];
	int linecount=0;
	int check=0;
	long logout_time;
	int screen_height=screen_h();

	file=userdb_open(O_RDONLY);
	if (file < 0)
		return;

	for_each_user(&usr, file, err) {
		if (type==0) check=urec->status;
		else
		if (type==1) check=urec->special;
		else
		if (type==2) check=urec->groups;
		else
		if (type==3) check=urec->chatmode;
		else
		if (type==4) check=urec->chatprivs;
		else
		if (type==5) check=(2 << ((urec->chatprivs&CP_PROTMASK) >> CP_PROTSHIFT))-1;
		else
		if (type==6) check=(2 << ((urec->chatmode&CM_PROTMASK) >> CM_PROTSHIFT))-1;

		check=check&flags;
		if (inv?check==0:check!=0)
		{
			logout_time=urec->lastlogout;
			show_user_stats(urec->status,stats,true);
			show_special(urec->special,spec,true);
			show_fold_groups(urec->groups,gr,true);
			show_chatmodes(urec->chatmode,cmodes,true);
			show_chatprivs(urec->chatprivs,cprivs,true);
			show_protection(urec->chatmode, urec->chatprivs, prot, 9);

			if (!u_del(&usr))
			{
				linecount++;

				if (type < 3)
					printf("%*s [%s] [%s] [%s] %s", NAMESIZE, urec->name,
					  stats, spec, gr, ctime(&logout_time));
				else
					printf("%*s [%s] [%s] [%s] %s", NAMESIZE, urec->name, cmodes,
					     cprivs, prot, ctime(&logout_time));

				if (linecount>=(screen_height-1))
				{
					char foo[6];
					printf(_("---more---\r"));
					echo_off();
					get_str(foo,5);
					echo_on();
					if (foo[0]=='q'|| foo[0]=='Q') break;
					printf("            \r");
					linecount=0;
				}
			}
		}
	}
	close(file);
}

void search(const char *args, const char *ptr)
{
	int data;
	int inv=0;
	int i;

	if (ptr[0]=='!')
	{
		inv=1;
		ptr++;
	}
	if (stringcmp(args,"status",2))
	{
		data=user_stats(ptr,0);
		list_users_flags(data,0,inv);
	}else
	if (stringcmp(args,"special",2))
	{
		data=set_special(ptr,0);
		list_users_flags(data,1,inv);
	}else
	if (stringcmp(args,"groups",1))
	{
		data=folder_groups(ptr,0);
		list_users_flags(data,2,inv);
	}else
	if (stringcmp(args,"chatmode",5))
	{
		data=cm_setbycode(0,ptr);
		list_users_flags(data,3,inv);
	}else
	if (stringcmp(args,"chatpriv",5))
	{
		data=cp_setbycode(0,ptr);
		list_users_flags(data,4,inv);
	}
	if (stringcmp(args,"protpower",5))
	{
		data=0;
		for (i=0;i<strlen(ptr);++i)
			if(isdigit(ptr[i]))
				data |= 1 << (ptr[i]-'0');
		list_users_flags(data,5,inv);
	}
	if (stringcmp(args,"protlevel",5))
	{
		data=0;
		for (i=0;i<strlen(ptr);++i)
			if(isdigit(ptr[i]))
				data |= 1 << (ptr[i]-'0');
		list_users_flags(data,6,inv);
	}
}

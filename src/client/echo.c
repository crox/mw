/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <stdio.h>
#include <termios.h>
#include <arpa/telnet.h>
/* POSIX echo handler  - Alan Cox 1993 */
#include "echo.h"

extern int internet;

static struct termios Old;
static short has_been_set=0;

void echo_on(void)
{
	if(internet)
		printf("%c%c%c",IAC,WONT,TELOPT_ECHO);
	else
	{
		if(has_been_set)
			tcsetattr(0,TCSANOW,&Old);
	}
}

void echo_off(void)
{
	if(internet)
	{
		printf("%c%c%c",IAC,WILL,TELOPT_ECHO);
		printf("%c%c%c",IAC,WILL,TELOPT_ECHO);
		fflush(stdout);
	}
	else
	{
		if(!has_been_set)
		{
			if(tcgetattr(0,&Old)!=-1)
				has_been_set=1;
		}
		if(has_been_set)
		{
			struct termios work;
			tcgetattr(0,&work);
			work.c_lflag&=~ICANON;
			work.c_lflag&=~ECHO;
			work.c_cc[VMIN]=1;
			work.c_cc[VTIME]=0;
			tcsetattr(0,TCSANOW,&work);
		}
	}
}

void force_line_mode(void)
{
/*
	printf("%c%c%c",IAC,DO,TELOPT_LINEMODE);
*/
}

static struct termios SavedTerm;

void save_terminal(void)
{
	tcgetattr(0, &SavedTerm);
}
void fix_terminal(void)
{
/*	struct termios work;
	tcgetattr(0,&work);
	work.c_lflag|=ICANON;
	work.c_lflag|=ECHO;
	work.c_lflag|=ICRNL;
	work.c_cc[VMIN]=1;
	work.c_cc[VTIME]=0;
	tcsetattr(0,TCSANOW,&work);
*/
	tcsetattr(0,TCSANOW,&SavedTerm);
}

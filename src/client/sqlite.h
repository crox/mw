#ifndef CLIENT_SQLITE_H
#define CLIENT_SQLITE_H

extern struct js_db_result* js_db_query(char *dbname, char *query);
extern void js_db_free(struct js_db_result *result);
extern char * userdb_get(const char *table, const char *user, const char *opt);
extern void userdb_set(const char *table, const char *user, const char *opt, const char *arg);

#endif /* CLIENT_SQLITE_H */

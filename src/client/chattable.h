#ifndef CHATTABLE_H
#define CHATTABLE_H

#include "Parse.h"

/* command string table, whilst in talker mode */
extern CommandList chattable[];

#endif /* CHATTABLE_H */

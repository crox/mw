#ifndef NONCE_H
#define NONCE_H

const char *get_nonce(void);
int match_nonce(const char *test);

#endif /* NONCE_H */

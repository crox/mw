[1mNAME[0m
     gag   - gag the user (with optional filter)
     ungag - Remove the gag on the user

[1mSYNOPSIS[0m
     [1mgag[0m [4mUSERNAME[0m [[4mFILTER[0m]
     [1mungag[0m [4mUSERNAME[0m

[1mDESCRIPTION[0m
     [1mgag[0m works on current talker users, and forces all of their text to be
     replaced with letter M's (as if they had a gag in their mouth)

     The optional [4mFILTER[0m parameter allows you to substitute a range of comical
     effects instead of the plain gag.

     [1mungag[0m removes the specified user's gag (if they're gagged).

     [4mFILTER[0m
     Name     Modes  Description
     -------------------------------------------------------------------------
     annoy    25     Various word/acronym substitutions,  e.g. "fish".
     babel    35     Babelfish (mock computer mistranslation).
     bunny    5      Replace every word with "boing".
     censor   4      Censors out swearwords.
     chef     2      Muppets Swedish Chef, Bork Bork Bork!
     duck     1234   Replace every word with "quack".
     french   125    Speck Onglish leek a Fronchman.
     fudd     3      Elmer Fudd style (Be vewy vewy quiet, I'm hunting wabbits).
     furby    134    Start speaking furby WAH!
     jive     13     Ghetto Jive talk.
     kraut    12     Mock german.
     latin    123    Pig Latin.
     mirror   23     Reverse the sentence.
     morse    15     Speak morse code.
     nosport  135    Be a good sport, sport, and don't mention sport okay?
     strfry   124    Randomly screws up your text, so all the chars are in the
                     wrong order.
     swab     24     Reverses the byte-order of the string, to mess it up.
     tnarg    34     Turn your text into Tnargian.
     warez    234    Speak w/\rez d00dz!
     wibble   14     Scrambles sentences to new and exotic ones.


[1;4mMiscellaneous Instructions[0m

[1mNAME[0m
     [1mLISTVARS[0m - Get the variable list (with optional argument)

[1mSYNOPSIS[0m
     [1mLISTVARS[0m [4mvariable[0m

[1mDESCRIPTION[0m
     Puts a string containing a compressed list of the variables used by your
     scripts into [4mvariable[0m. The compression works by sorting the
     variables into alphabetical order, then, for each variable name, it
     removes any common characters with the previous variable from the start,
     and replaces them with a character telling you how many characters are
     repeated. The characters used are 0-9A-Za-z to indicate 0-61 repeated
     characters.

     For example the variable names:
       test, test_all, trick, tyrant, zephyr_longstring1, zephyr_longstring2
     translate to the string
       "0test 4_all 1rick 1yrant 0zephyr_longstring1 H2"


     To decompress the string, you can use the something like the following
     code. The area within the **** comments is where you can do what you
     want with the variable name:

         function decompress_listvars
             # set up all local variables
             LOCAL varlist name prevname compchar

             # get compressed form of variable list
             LISTVARS varlist

             # set up previous name
             SET prevname ""

             # start name stripping iteration loop
           loop:
             # get variable name off from list
             SPLIT $varlist name varlist

             # if nothing left to strip off, exit the loop
             IFCLEAR
             EQ $name ""
             IFALL endloop

             # strip off first character as the compression char
             STRMID compchar 0:0 $name
             STRMID name 1: $name

             # convert the compression char into a number
             STRCHR compchar $compchar "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

             # get this many characters from the previous variable name
             STRMID compchar 0:$[compchar-1] $prevname

             # append the rest of the string name
             SET name ${compchar}${name}

             #************************************
             PRINT "${name}"
             #************************************

             # set the new previous word
             SET prevname $name

             # continue the loop
             JUMP loop

           endloop:
             # no more variable names left
         endfunc

